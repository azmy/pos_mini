-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;



-- Dumping structure for table dw_caffee.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `menu_id` int(11) unsigned DEFAULT NULL,
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dw_caffee.module: ~5 rows (approximately)
DELETE FROM `module`;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `menu_id`, `slug`, `active`) VALUES
	(2, 'Halaman Module', 'Edit Users', 'edit existing users', '2019-07-05 10:16:26', '2019-07-20 04:27:09', 105, 'admin/module', 1),
	(3, 'Halaman Users', 'create,edit,delete', 'Page User', '2019-07-06 00:00:00', '2019-07-20 04:22:02', 104, 'admin/user', 1),
	(6, 'Dashboard', NULL, 'Halaman Home', '2019-07-23 00:29:30', '2019-07-23 00:29:36', 101, 'admin', 1),
	(7, 'Hak Akses', NULL, 'Halaman Hak Akses User', '2019-07-23 00:31:14', '2019-07-23 00:31:19', 102, '#', 1),
	(8, 'Halaman Role Access', NULL, 'ok', '2019-07-31 07:24:01', '2019-07-31 07:24:01', 103, 'admin/role', 1);
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
