<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu_role extends Model
{
    protected $table = "menu_role";

    public function role()
    {
        
        return $this->belongsTo(Role::class);
    }

}
