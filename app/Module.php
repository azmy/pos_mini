<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laratrust\Models\LaratrustPermission;
use App\Permission_user;

class Module extends Model{
    

    protected $table = "module";

    public function permission_users()
    {
        
        return $this->hasMany(Permission_user::class);
    }

    public function permission_roles()
    {
        
        return $this->hasMany(Permission_role::class);
    }

   

}
