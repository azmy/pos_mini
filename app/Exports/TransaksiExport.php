<?php

namespace App\Exports;

use App\Transaksi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TransaksiExport implements FromView
{
    protected $currentDate;
    protected $startDate;
    protected $finalRow;

    public function __construct($startDate,$currentDate)
    {
        $this->currentDate = $currentDate;
        $this->startDate = $startDate;
    }

    public function view(): View
    {
        $transactions = Transaksi::query()
        ->where('dibayar','>',0)
        ->whereBetween('tgl_trans',[$this->startDate, $this->currentDate])
        ->get();
        
        return view('reports.spreadsheet', [
            'transactions' => $transactions,
            'startDate'=>$this->startDate,
            'currentDate'=>$this->currentDate
        ]);
    }

}
