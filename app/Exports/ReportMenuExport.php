<?php

namespace App\Exports;

use App\Services\CategoryService;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;

class ReportMenuExport implements  FromCollection, WithCustomStartCell,
WithHeadings,WithColumnWidths,WithProperties,WithEvents
{
    
    use  Exportable, RegistersEventListeners;

    protected $currentDate;
    protected $startDate;
    protected $finalRow;
    protected static $stDate;
    protected static $crDate;
    protected static $fnRow;

    public function __construct($startDate,$currentDate)
    {
        $this->currentDate = $currentDate;
        $this->startDate = $startDate;
        self::$stDate=$startDate;
        self::$crDate = $currentDate;
    }

    public function headings(): array
    {
        return [
            'No', 'Menu Makanan', 'Kuantitas','Jumlah Uang'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'B' => 5,
            'C' => 35,
            'D'=>15,
            'E'=>25            
        ];
    }

    public function properties(): array
    {
        return [
            'title'          => 'Laporan Penjualan Per Menu ' . tanggal_indonesia($this->startDate) . " s/d " . tanggal_indonesia($this->currentDate),
            'description'    => 'Penjualan Per Menu',
            'subject'        => 'Penjualan',
            'keywords'       => 'Penjualan,export,spreadsheet',
            'category'       => 'Penjualan',
            'manager'        => 'Patrick Brouwers',
            'company'        => 'Maatwebsite',
        ];
    }


    public function collection()
    {
        $transactions = (new CategoryService())->getMenu($this->startDate,$this->currentDate);
        
        $test =  new Collection([]);
        $i=1;
        $tqty=0;
        $tjumlah=0;

        foreach($transactions as $t)
        {
            $trans = array();
            array_push($trans,$i);
            array_push($trans,$t->nama);
            array_push($trans,$t->qty);
            array_push($trans,$t->total);
            $test->push($trans);
            $tqty+=$t->qty ?? 0;
            $tjumlah+=$t->total;
            $i++;
            
        }
        $trans=array();
        array_push($trans,'');
        array_push($trans,'TOTAL');
        array_push($trans,$tqty);
        array_push($trans,$tjumlah);
        $test->push($trans);
        $this->finalRow = $i + 6;
        self::$fnRow=$i  + 6;
        return $test;
    }
    public function startCell(): string
    {
        return 'B6';
    }

    public static function afterSheet(AfterSheet $event) 
    {
        $event->sheet->getDelegate()->getCell('B3')
        ->setValue('Laporan Penjualan per Menu periode tanggal '. tanggal_indonesia(self::$stDate,false) . " s/d " .tanggal_indonesia(self::$crDate,false) );

        $event->sheet->getDelegate()->getStyle('B3')->getFont()->setBold(true);
        
        $event->sheet->getDelegate()->getStyle('B6:E'.self::$fnRow)
        ->getBorders()
        ->getAllBorders()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('FF000000'));
    }
}
