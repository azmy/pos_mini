<?php

namespace App\Exports;

use App\Transaksi;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;



class UsersExport implements FromCollection, WithCustomStartCell,
WithHeadings,WithColumnWidths,WithProperties,WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use  Exportable, RegistersEventListeners;

    protected $currentDate;
    protected $startDate;
    protected $finalRow;
    protected static $stDate;
    protected static $crDate;
    protected static $fnRow;

    public function __construct($startDate,$currentDate)
    {
        $this->currentDate = $currentDate;
        $this->startDate = $startDate;
        self::$stDate=$startDate;
        self::$crDate = $currentDate;
    }

    public function headings(): array
    {
        return [
            'No', 'Tanggal Transaksi', 'Kode Transaksi','Diskon','Pajak','Total','Dibayar'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'B' => 5,
            'C' => 20,
            'D'=>35            
        ];
    }

    public function properties(): array
    {
        return [
            'title'          => 'Laporan Penjualan ' . tanggal_indonesia($this->startDate) . " s/d " . tanggal_indonesia($this->currentDate),
            'description'    => 'Latest Invoices',
            'subject'        => 'Invoices',
            'keywords'       => 'invoices,export,spreadsheet',
            'category'       => 'Invoices',
            'manager'        => 'Patrick Brouwers',
            'company'        => 'Maatwebsite',
        ];
    }


    public function collection()
    {
        $transactions = Transaksi::query()
        ->where('dibayar','>',0)
        ->whereBetween('tgl_trans',[$this->startDate, $this->currentDate])
        ->get();

        $test =  new Collection([]);
        $i=1;
        $total=0;
        $dibayar=0;
        $diskon=0;
        $pajak=0;

        foreach($transactions as $t)
        {
            $trans = array();
            array_push($trans,$i);
            array_push($trans,tanggal_indonesia($t->tgl_trans,false));
            array_push($trans,$t->kode);
            array_push($trans,$t->diskon_persen ?? 0);
            array_push($trans,$t->pajak_persen ?? 0);
            array_push($trans,$t->total_grand ?? 0);
            array_push($trans,($t->dibayar - $t->kembali) ?? 0 );
            $test->push($trans);
            $total+=$t->total_grand ?? 0;
            $dibayar+=($t->dibayar - $t->kembali) ?? 0;
            $i++;
            
        }
        $trans=array();
        array_push($trans,'');
        array_push($trans,'TOTAL');
        array_push($trans,'');
        array_push($trans,'');
        array_push($trans,'');
        array_push($trans,$total);
        array_push($trans,$dibayar);
        $test->push($trans);
        $this->finalRow = $i + 6;
        self::$fnRow=$i  + 6;
        return $test;
    }
    public function startCell(): string
    {
        return 'B6';
    }

    public static function afterSheet(AfterSheet $event) 
    {
        $event->sheet->getDelegate()->getCell('B3')
        ->setValue('Laporan tanggal '. tanggal_indonesia(self::$stDate,false) . " s/d " .tanggal_indonesia(self::$crDate,false) );

        $event->sheet->getDelegate()->mergeCells('C'.self::$fnRow.':F'.self::$fnRow);

        $event->sheet->getDelegate()->getStyle('B3')->getFont()->setBold(true);
        
        $event->sheet->getDelegate()->getStyle('B6:H'.self::$fnRow)
        ->getBorders()
        ->getAllBorders()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('FF000000'));
    }
}
