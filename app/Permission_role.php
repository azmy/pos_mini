<?php

namespace App;
use App\Permission;
use App\Users;

use Illuminate\Database\Eloquent\Model;

class Permission_role extends Model
{
    protected $table = "permission_role";

    public function users()
    {
        return $this->belongsTo(Users::class);
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
