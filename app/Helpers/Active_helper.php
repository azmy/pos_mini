<?php
if (!function_exists('set_active')) {
function set_active($uri, $output = 'active')
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}
}

if (!function_exists('set_child_active')) {
    function set_child_active($uri, $output = 'display: block;color:#fff;')
    {
     if( is_array($uri) ) {
       foreach ($uri as $u) {
         if (Route::is($u)) {
           return $output;
         }
       }
     } else {
       if (Route::is($uri)){
         return $output;
       }
     }
    }
    }

    if (!function_exists('set_current_child')) {
        function set_current_child($uri, $output = 'current_page')
        {
         if( is_array($uri) ) {
           foreach ($uri as $u) {
             if (Route::is($u)) {
               return $output;
             }
           }
         } else {
           if (Route::is($uri)){
             return $output;
           }
         }
        }
        }
?>