<?php

use App\Module;
use App\Permission_role;
use Illuminate\Http\Request;

if (!function_exists('can_create')) {
    function can_create()
    {

        if (Auth::user()->role_id == 1) {
            return true;
        } else {
            $slug = array();
            $acces = array();
            $current_url = request()->segment(1) . "/" . request()->segment(2);
            if ($current_url == "admin/" or request()->ajax()) {
                return $next($request);
            } else {
                $permission = Module::where('slug', $current_url)->first();
                $module = Permission_role::where(array('module_id' => $permission->id, 'can_create' => 1, 'role_id' => Auth::user()->role_id))->first();
                $slug[] = @$permission->slug;
                $acces[] = @$module->can_create;
                $can_acces = 1;
                if (in_array($can_acces, $acces, true)) {
                    return true;
                } elseif ($current_url == "admin/" or request()->ajax()) {
                    return true;
                } else {
                    redirect()->to('admin/404')->send();
                    exit;
                }

            }

        }

    }
}

if (!function_exists('can_edit')) {
    function can_edit()
    {

        if (Auth::user()->role_id == 1) {
            return true;
        } else {
            $slug = array();
            $acces = array();
            $current_url = request()->segment(1) . "/" . request()->segment(2);
            if ($current_url == "admin/" or request()->ajax()) {
                return true;
            } else {
                $permission = Module::where('slug', $current_url)->first();
                $module = Permission_role::where(array('module_id' => $permission->id, 'can_edit' => 1, 'role_id' => Auth::user()->role_id))->first();
                $slug[] = @$permission->slug;
                $acces[] = @$module->can_edit;
                $can_acces = 1;
                if (in_array($can_acces, $acces, true)) {
                    return true;
                } elseif ($current_url == "admin/" or request()->ajax()) {
                    return true;
                } else {
                    redirect()->to('admin/404')->send();
                    exit;
                }

            }

        }

    }
}

if (!function_exists('can_delete')) {
    function can_delete()
    {
        if (Auth::user()->role_id == 1) {
            return true;
        } else {
            $slug = array();
            $acces = array();
            $current_url = request()->segment(1) . "/" . request()->segment(2);
            if ($current_url == "admin/") {
                return true;
            } else {
                $permission = Module::where('slug', $current_url)->first();
                $module = Permission_role::where(array('module_id' => $permission->id, 'can_delete' => 1, 'role_id' => Auth::user()->role_id))->first();
                $slug[] = @$permission->slug;
                $acces[] = @$module->can_delete;
                $can_acces = 1;
                if (in_array($can_acces, $acces, true)) {
                    return true;
                } elseif ($current_url == "admin/") {
                    return true;
                } else {
                    redirect()->to('admin/404')->send();
                    exit;
                }

            }

        }

    }
}
