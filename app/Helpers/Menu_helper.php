<?php
use App\Menu;
if (!function_exists('menu')) {
    function menu()
    {
        if (@Auth::user()->role_id == 1) {
            $menu_0 = Menu::get();
            foreach ($menu_0 as $key) {
                get_menu_child($key->id);
            }
        } else {
            $menu_id = App\Menu_role::where(array('role_id' => @Auth::user()->role_id))->first();
            foreach (explode(',', @$menu_id->menu_id) as $v) {
                $menu_0 = Menu::where(array('id' => $v, 'id_induk' => 0))->orderBy('order', 'ASC')->get();
                foreach ($menu_0 as $key) {
                    if ($key->id == $v) {
                        get_menu_child($key->id);
                    } else {
                        return false;
                    }
                }

            }

        }

    }

    function get_menu_child($parent = 0)
    {

        $menu = Menu::where(array('id_induk' => $parent))->orderBy('order', 'ASC')->get();
        $parent = Menu::where('id', $parent)->orderBy('order', 'ASC')->first();

        echo "<li>";
        echo "<a href='" . url($parent->url) . "'>";
        echo "<i class='fa " . $parent->icon . "'></i>";

        echo "<span>$parent->nama</span>";

        if (sizeof($menu) > 0) {
            echo "<span class='fa fa-chevron-down'></span>";
        }
        echo "</a>";
        if (sizeof($menu) > 0) {
            $current_url = request()->segment(2) . "." . request()->segment(3);
            $block = '';
            if (Request::route()->getName() == $current_url) {
                $block = "display:block";
            }
            echo "<ul class='nav child_menu' style='" . $block . "'>";
            foreach ($menu as $key) {
                $menu_id = App\Menu_role::where(array('role_id' => @Auth::user()->role_id))->first();
                foreach (explode(',', @$menu_id->menu_id) as $v) {
                    if ($v == $key->id) {
                        get_menu_child($key->id);
                    }
                }

            }

            echo "</ul>";

        }
        echo "</li>";
    }

}

if (!function_exists('admin')) {
    function admin()
    {

        $menu_0 = Menu::where('id_induk', 0)->orderBy('order', 'ASC')->get();
        foreach ($menu_0 as $key) {
            get_menu_child_admin($key->id);
        }

    }

    function get_menu_child_admin($parent = 0)
    {

        $menu = Menu::where('id_induk', $parent)->orderBy('order', 'ASC')->get();
        $parent = Menu::where('id', $parent)->first();

        echo "<li>";
        echo "<a href='" . url($parent->url) . "'>";
        echo "<i class='fa " . $parent->icon . "'></i>";

        echo "<span>$parent->nama</span>";

        if (sizeof($menu) > 0) {
            echo "<span class='fa fa-chevron-down'></span>";
        }
        echo "</a>";
        if (sizeof($menu) > 0) {
            $current_url = request()->segment(2) . "." . request()->segment(3);
            $block = '';
            if (Request::route()->getName() == $current_url) {
                $block = "display:block";
            }
            echo "<ul class='nav child_menu' style='" . $block . "'>";
            foreach ($menu as $key) {

                get_menu_child_admin($key->id);

            }
            echo "</ul>";

        }

        echo "</li>";
    }

}
