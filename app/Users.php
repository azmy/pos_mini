<?php

namespace App;
use App\Permission_user;
use App\Roles_user;
use App\Role;
use App\User_group;
use Illuminate\Database\Eloquent\Model;
use Laratrust\Traits\LaratrustUserTrait;





class Users extends Model
{

    use LaratrustUserTrait;

    protected $table = "users";

    protected $fillable = [
        'id','name', 'email', 'password','role_id','created_at','updated_at'
    ];

    protected $hidden = ['password', 'token'];

    
    public function roles_users()
    {
    	return $this->hasMany(Roles_user::class);
    }

    public function roles(){
        return $this->belongsToMany(Role::class, 'users', 'role_id');
    }

    public function permission_users()
    {
        
        return $this->hasMany(Permission_user::class);
    }

    public function user_group()
    {
    	return $this->belongsTo(User_group::class,'group_id');
    }

    public function permission_role()
    {
    	return $this->hasMany(Permission_role::class,'permission_id');
    }
 
}
