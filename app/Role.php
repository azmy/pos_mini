<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laratrust\Models\LaratrustRole;
use App\Permission;
use App\Permission_role;
use App\Roles_user;

class Role extends LaratrustRole
{
    protected $table = "role";

    
    
    public function roles_users()
    {
        //return $this->belongsToMany('App\Roles_user');
        return $this->hasMany(Roles_user::class);
    }

    public function users()
    {
       
        return $this->hasMany(Users::class);
    }

    public function permission_role()
    {
    	return $this->hasMany(Permission_role::class,'role_id');
    }

    public function menu_role()
    {
    	return $this->hasMany(Menu_role::class);
    }

    
}
