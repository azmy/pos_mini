<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Users;
use App\Permission;

class Permission_user extends Model
{
    protected $table = "permission_user";

    protected $fillable = [
        'permission_id','user_id'
    ];

    public function users()
    {
        return $this->belongsTo(Users::class);
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

}
