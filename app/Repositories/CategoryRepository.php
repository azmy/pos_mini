<?php 
namespace App\Repositories;

use App\MenuMakan;

class CategoryRepository {

    public function getMenu($dari,$sampai)
    {
        return \DB::table('detail_trans as  a')
            ->leftJoin('transaksi as b',function($j){
                $j->on('b.kode','a.kode_trans');
            })
            ->leftJoin('menu_mkn as c',function($j){
                $j->on('a.kode_menu','c.kode');
            })
            ->select(\DB::raw("c.id, c.nama, count(a.kode_menu) qty, sum(b.dibayar) total"))
            ->whereBetween(\DB::raw('date(b.tgl_trans)'),[$dari,$sampai])
            ->groupBy('a.kode_menu')
            ->orderBy('c.kode','ASC')
            ->get();
    }
}