<?php 
namespace App\Repositories;

use DB;
use Carbon\Carbon;

class TransactionRepository {

    public function monthly() {
        return DB::table('transaksi')
        ->select(DB::raw('month(tgl_trans) bulan, sum(dibayar - kembali) total'))
        ->whereYear('tgl_trans',Carbon::now()->format('Y'))
        ->groupBy(DB::raw("month(tgl_trans)"))
        ->get();
    }
}