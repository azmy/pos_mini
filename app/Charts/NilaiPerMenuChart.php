<?php

namespace App\Charts;

use App\MenuMakan;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class NilaiPerMenuChart extends Chart
{
    protected $url;
    public function __construct($url="")
    {
        $this->setUrl($url);
        parent::__construct();
    }

    public function setUrl($url){
        $this->url = $url;
    }

    public function diplay(){
        $menuApi = Url($this->url);
        $menus = MenuMakan::select('nama')->get();
        $header = array_column($menus->toArray(), 'nama');
        $this->labels($header)->load($menuApi);
    }
}
