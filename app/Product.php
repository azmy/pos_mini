<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "produk";
  
    public function kategori() {
        return $this->belongsTo('App\Kategori', 'id_kategori');
    }

}
