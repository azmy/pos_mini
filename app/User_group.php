<?php

namespace App;
use App\Users;

use Illuminate\Database\Eloquent\Model;

class User_group extends Model
{
    protected $table = "user_group";

    public function users()
    {
    	return $this->hasMany(Users::class,'id');
    }

}
