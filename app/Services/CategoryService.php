<?php
namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService {
    

    public function getMenu($dari,$sampai)
    {
        return (new CategoryRepository)->getMenu($dari,$sampai);
    }
}