<?php
namespace App\Services;

use App\MenuMakan;
use App\Repositories\CategoryRepository;
use App\Repositories\TransactionRepository;

class ChartTransactionService {

    public function monthly() : array {
        $test = (new TransactionRepository())->monthly();
        $blnArray = [0,0,0,0,0,0,0,0,0,0,0,0];
        foreach($test as $t)
        {
            $index = $t->bulan -1;
            $blnArray[$index]=$t->total;
        }

        return $blnArray;
    }

    public function monthlyPerMenu($type='qty',$dari,$sampai) : array {
        $test = (new CategoryRepository)->getMenu($dari,$sampai);
        
        $menus = MenuMakan::select('id')->get();
        $header = array_column($menus->toArray(), 'id');
        $qty=array();
        $nilai=array();
       
        foreach($header as $h)
        {
            $key = array_search($h, $header);
            $qty[$key]=0;
            $nilai[$key]=0;
        }
        
        foreach($test as $m)
        {
            $key = array_search($m->id, $header);
            $qty[$key]=$m->qty;
            $nilai[$key]=$m->total;
        }
        if($type=='nilai')
        {
            return $nilai;
        }
        else
        {
            return $qty;
        }
    }
}