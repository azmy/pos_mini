<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permission;
use App\Permission_user;
use App\Roles_user;
use App\Users;

class Roles_user extends Model
{
    protected $table = "role_user";
    
    public function user()
    {
        //return $this->belongsToMany('App\User');
        return $this->belongsTo(User::class);
    }

    public function role()
    {
        //return $this->belongsTo('App\Role');
        return $this->belongsTo(Role::class);
    }

    public function roles_users()
    {
    	return $this->belongsTo('App\Roles_user');
    }

}
