<?php

namespace App\Http\Controllers\BackEnd\GantiPassword;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Users;
use Auth;

class GantiPasswordController extends Controller
{
    public function index(Request $req){
        return view('ganti_password.form');
    }

    public function save(Request $req){
        $validator = Validator::make($req->all(),

        array(
            'password_lama' => 'required',
            'password_baru' => 'required|min:5',
            'konf_password' => 'required|same:password_baru',
        ),

        array(
            'password_lama.required' => 'password tidak boleh kosong',
            'password_baru.required' => 'password baru tidak boleh kosong',
            'konf_password.same'     => 'Konfirmasi password tidak sama',
            'konf_password.required' => 'Konfirmasi password baru tidak boleh kosong',
        )
    );

    //Kondisi validasi (Pesan Error yang akan terjadi)
    if ($validator->fails()) {
          return redirect(route('gantipassword.index'))->withErrors($validator);
        
    }else{

        $user = Users::find(Auth::user()->id);
        if (Hash::check($req->password_lama, $user->password)) {
        //saving logic here
         $save = Users::where('id', Auth::user()->id)->update(array('password' => Hash::make($req->konf_password)));
        
        }else{
            return redirect(route('gantipassword.index'))->with('gagal', 'Password lama tidak sama');
        }
        if($save){
            return redirect(route('gantipassword.index'))->with('sukses', 'Password berhasil diganti');
        }else{
            return redirect(route('gantipassword.index'))->with('gagal', 'Password gagal diganti');
        }

    }   
    }
}
