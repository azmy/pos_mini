<?php

namespace App\Http\Controllers\BackEnd\Kategori;

use Illuminate\Http\Request;
use App\Http\Controllers\BackEndController;
use App\Kategori;
use DataTables;
use Validator;

class KategoriController extends BackEndController
{
   public function index(Request $req){
    return view('kategori.index');
   }

   public function jsondata()
    {

        $data = Kategori::get();
        return Datatables::of($data)

            ->addColumn('x', function ($data) {
                return '<input name="id[]" value="' . $data->id . '" class="check-item" type="checkbox">';
            })

            ->addColumn('no', function ($data) {
                return '';
            })
            ->addColumn('kategori', function ($data) {
                return $data->nama;
            })
            ->addColumn('action', function ($data) { 
             
                    return '
                        <a href="' . route('kategori.edit', $data->id) . '" class="btn btn-sm btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="#" class="btn btn-delete btn-sm btn-sm btn-danger" data-id="' . $data->id . '"><i class="fa fa-trash"></i></a>';     
            })
            ->rawColumns(['x', 'action'])
            ->make(true);

    }

    public function add(){
        can_create();
        return view('kategori.form');
    }

    public function edit(Request $req){
        can_edit();
        $data['kategori'] = Kategori::find($req->id);
        return view('kategori.form', $data);
    }

    public function save(Request $req){
        $validator = Validator::make($req->all(),

        array(
            'kategori' => 'required'
        ),

        array(
            'kategori.required' => 'kategori tidak boleh kosong'
        )
    );

    //Kondisi validasi (Pesan Error yang akan terjadi)
    if ($validator->fails()) {
        if($req->id!=null){
        return redirect(route('kategori.edit', $req->id))->withErrors($validator);
        }else{
        return redirect(route('kategori.add'))->withErrors($validator);
        }
    }else{
      
        $kategori   = new Kategori;

        if ($req->id != null) {
        $kategori           = Kategori::find($req->id);
        $kategori->exists   = true;
        }
        $kategori->nama = $req->kategori;
        $save = $kategori->toSql();
        dd($save);
        
        if($save){
            return redirect(route('kategori.index'))->with('sukses', 'Data berhasil tersimpan');
        }else{
            return redirect(route('kategori.index'))->with('gagal', 'Data berhasil tersimpan');
        }

    }   

    }

    public function delete(Request $req){
        can_delete();
        Kategori::where('id', $req->id)->delete();
    }

    public function delete_all(Request $req){
        can_delete();
        if ($req->id) {
            Kategori::whereIn('id', $req->id)->delete();
            return redirect(route('kategori.index'))->with('sukses', 'Data berhasil dihapus');
        } else {
            return redirect(route('kategori.index'))->with('gagal', 'Data gagal dihapus');
        }
    }

}
