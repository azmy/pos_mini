<?php

namespace App\Http\Controllers\BackEnd\Users;

use App\Http\Controllers\BackEndController;
use App\Role;
use App\Users;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends BackEndController
{
    public function index()
    {
        return view('users.index');
    }

    public function jsondata()
    {
        $data = DB::table('users')
            ->leftJoin('roles', 'roles.id', '=', 'users.role_id')
            ->get(['users.name', 'users.username', 'email', 'users.id', 'active', 'roles.name as group_name']);

        /* $data = Users::with('roles')->get(); */

        return Datatables::of($data)

            ->addColumn('x', function ($data) {
                return '<input name="id[]" value="' . $data->id . '" class="check-item" type="checkbox">';
            })
            ->addColumn('No', function ($data) {
                return '';
            })
            ->addColumn('name', function ($data) {
                return $data->name;
            })
            ->addColumn('username', function ($data) {
                return $data->username;
            })
            ->addColumn('email', function ($data) {
                return $data->email;
            })
            ->addColumn('group_name', function ($data) {
                return $data->group_name;
            })

            ->addColumn('active', function ($data) {

                if ($data->active == 1) {
                    return 'active';
                } else {
                    return 'not active';
                }

            })

            ->addColumn('action', function ($data) {
                return '<a href="' . route('user.edit', $data->id) . '" class="btn btn-sm btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="#" class="btn btn-delete btn-sm btn-sm btn-danger" data-id="' . $data->id . '"><i class="fa fa-trash"></i></a>';
            })
            ->rawColumns(['x', 'action'])

            ->make(true);

    }

    public function add()
    {
        can_create();
        $data['role'] = Role::get();
        return view('users.form', $data);

    }

    public function save(Request $req)
    {

        $validator = Validator::make($req->all(),

            array(
                'username' => 'required|min:4',
                'email' => 'required',
                'level' => 'required',
            ),

            array(
                'username.required'     => 'username tidak boleh kosong',
                'username.min'          => 'username harus 4 karakter',
                'email.required'        => 'Email tidak boleh kosong',
                'email.email'           => 'Email tidak valid',
                'level.required'        => 'Level tidak boleh kosong',
            )
        );

        if ($req->id == null) {
            $validator = Validator::make($req->all(),

                array(
                    'username' => 'required|min:4',
                    'password' => 'required|min:5',
                    'konf_password' => 'required|same:password',
                    'email' => 'required|email|unique:users,email',
                ),

                array(
                    'username.required'     => 'username tidak boleh kosong',
                    'password.required' => 'Password tidak boleh kosong',
                    'password.min' => 'password minimal 5 karakter',
                    'konf_password.required' => 'Konfirmasi password tidak boleh kosong',
                    'konf_password.same' => 'Konfirmasi password tidak sama',
                    'email.unique' => 'Email telah digunakan',
                    'email.required'        => 'Email tidak boleh kosong'

                )
            );
        }

        /*  Kondisi validasi (Pesan Error yang akan terjadi) */
        if ($validator->fails()) {
            if ($req->id != null) {
                return redirect(route('user.edit', $req->id))->withErrors($validator);
            } else {
                return redirect(route('user.add'))->withErrors($validator);
            }
        } else {

            $user = new Users;

            if ($req->id != null && $req->ganti_pass == 1) {
                $user = Users::find($req->id);
                $user->exists = true;
                $user->password = Hash::make($req->password);
            } elseif ($req->id != null && $req->ganti_pass == null) {
                $user = Users::find($req->id);
                $user->password = @$user->password;
            } else {
                $user->password = Hash::make($req->password);
            }

            if ($req->active != null) {
                $active = '1';
            } else {
                $active = '0';
            }

            $user->name = $req->name;
            $user->username = $req->username;
            $user->email = $req->email;
            $user->role_id = $req->level;
            $user->active = $active;

            $user = $user->save();
            if ($user) {
                return redirect(route('user.users'))->with('sukses', 'Data berhasil tersimpan');
            } else {
                return redirect(route('user.users'))->with('gagal', 'Data gagal tersimpan');
            }

        }

    }

    public function delete($id)
    {
        can_delete();
        $role = Users::where('id', $id)->delete();
        return redirect(route('user.users'))->with('sukses', 'Data berhasil dihapus');
    }

    public function delete_all(Request $req)
    {
        can_delete();
        if ($req->id) {
            $role = Users::whereIn('id', $req->id)->delete();
            return redirect(route('user.users'))->with('sukses', 'Data berhasil dihapus');
        } else {
            return redirect(route('user.users'))->with('gagal', 'Data gagal dihapus');
        }
    }

    public function edit($id)
    {
        can_edit();
        $data['user'] = Users::find($id);
        $data['role'] = Role::get();
        return view('users.form', $data);
    }

}
