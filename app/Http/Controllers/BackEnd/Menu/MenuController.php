<?php

namespace App\Http\Controllers\BackEnd\Menu;

use App\Http\Controllers\BackEndController;
use App\Menu;
use DataTables;
use Illuminate\Http\Request;
use Validator;

class MenuController extends BackEndController
{

    public function index()
    {
        return view('menu.index');
    }

    public function jsondata()
    {
        $data = Menu::orderBy('id_induk', 'ASC')->get();

        return Datatables::of($data)

            ->addColumn('x', function ($data) {
                return '<input name="id[]" value="' . $data->id . '" class="check-item" type="checkbox">';
            })
            ->addColumn('No', function ($data) {
                return '';
            })
            ->addColumn('id', function ($data) {
                return $data->id;
            })

            ->addColumn('id_induk', function ($data) {
                return $data->id_induk;
            })

            ->addColumn('nama', function ($data) {
                return $data->nama;
            })

            ->addColumn('icon', function ($data) {
                return $data->icon;
            })

            ->addColumn('url', function ($data) {
                return $data->url;
            })

            ->addColumn('order', function ($data) {
                return $data->order;
            })

            ->addColumn('action', function ($data) {
                return '<a href="' . route('menu.edit', $data->id) . '" class="btn btn-xs btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a> <a href="#" data-id="'.$data->id.'" class="btn delete_account btn-delete btn-sm btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->rawColumns(['x', 'action'])
            ->make(true);

    }

    public function add()
    {
        can_create();
        return view('menu.form');
    }

    public function save(Request $req)
    {
        $validator = Validator::make($req->all(),

            array(
                'parent_id' => 'required|numeric',
                'name' => 'required',
                'slug' => 'required',
                'order' => 'required|numeric',

            ),

            array(
                'parent_id.required' => 'parent id tidak boleh kosong',
                'parent_id.numeric' => 'parent id harus angka',
                'name.required' => 'name tidak boleh kosong',
                'slug.required' => 'slug tidak boleh kosong',
                'order.required' => 'order tidak boleh kosong',
                'order.numeric' => 'order harus angka',
            )
        );

        //Kondisi validasi (Pesan Error yang akan terjadi)
        if ($validator->fails()) {
            if ($req->id != null) {
                return redirect(route('menu.edit', $req->id))->withErrors($validator);
            } else {
                return redirect(route('menu.add'))->withErrors($validator);
            }
        } else {

            $menu = new Menu;

            if ($req->id != null) {
                $menu = Menu::find($req->id);
                $menu->exists = true;
            }

            $menu->id_induk = $req->parent_id;
            $menu->nama = $req->name;
            $menu->icon = $req->icon;
            $menu->url = $req->slug;
            $menu->order = $req->order;

            $save = $menu->save();
            if ($save) {
                return redirect(route('menu.menus'))->with('sukses', 'Data berhasil tersimpan');
            } else {
                return redirect(route('menu.add'))->with('gagal', 'Data gagal tersimpan');
            }

        }
    }

    public function delete(Request $req)
    {
        can_delete();
        $menu = Menu::where('id', $req->id)->delete();
    }

    public function delete_all(Request $req){
        can_delete();
        if ($req->id) {
            Menu::whereIn('id', $req->id)->delete();
            return redirect(route('menu.menus'))->with('sukses', 'Data berhasil dihapus');
        } else {
            return redirect(route('menu.menus'))->with('gagal', 'Data gagal dihapus');
        }
    }

    public function edit($id)
    {
        can_edit();
        $data['menu'] = Menu::find($id);
        return view('menu.form', $data);
    }

    public function search_slug($id)
    {
        $slug = Menu::where('id', $id)->first();
        print $slug->url;
    }

}
