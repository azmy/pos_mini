<?php

namespace App\Http\Controllers\BackEnd\AccesDenied;
use App\Http\Controllers\BackEndController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AccesDeniedController extends BackEndController
{

    public function __construct()
    {
        //error_reporting(0);
    }


    public function index(Request $request){
        return view('404.index');
    }
}
