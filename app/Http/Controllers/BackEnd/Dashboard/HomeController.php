<?php

namespace App\Http\Controllers\BackEnd\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends  Controller
{    
    public function index(Request $req){
        return view('dashboard.index');
    }

   
}
