<?php

namespace App\Http\Controllers\BackEnd\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\BackEndController;
use Illuminate\Support\Str;
use DataTables;
use Validator;
use App\Kategori;
use App\Product;

class ProductController extends BackEndController
{


    public function index(Request $req){
        return view('product.product');
    }

    public function jsondata(Request $req){
        $data = Product::with('kategori')->get();
      
        return Datatables::of($data)

            ->addColumn('x', function ($data) {
                return '<input name="id[]" value="' . $data->id . '" class="check-item" type="checkbox">';
            })

            ->addColumn('no', function ($data) {
                return '';
            })
           
            ->addColumn('kategori', function ($data) {
                return $data->kategori->nama;
            })

            ->addColumn('nama', function ($data) {
                return $data->nama;
            })

            ->addColumn('deskripsi', function ($data) {
                return Str::limit($data->deskripsi, 80);
            })

            ->addColumn('harga', function ($data) {
                return 'Rp. '.format_uang($data->harga);
            })
          
            ->addColumn('foto', function ($data) {
                if(!empty($data->foto)){
                    $foto = $data->foto;
                }else{
                    $foto = 'no-image.png';
                }
                return '<image class="me-2" src="'.asset('assets/images/product_images').'/'.$foto.'"  alt="image" style="width:80px !important;height:80px !important;">';
            })
           
            ->addColumn('action', function ($data) { 
             
                    return '
                        <a href="#" class="btn btn-delete btn-sm btn-sm btn-danger" data-id="' . $data->id . '"><i class="fa fa-trash"></i></a>';     
            })
            ->rawColumns(['x','foto','deskripsi','action'])
            ->make(true);
    }

    public function add(Request $req){
        $data['kategori'] = Kategori::get();
        return view('product.form', $data);
    }

    public function edit(Request $req){
        return view('product.form');
    }

   
    public function daftar_product(Request $req){
        $data['kategori'] = Kategori::get();
        return view('product.daftar_product', $data);
    }

    public function delete(Request $req){
        can_delete();
        Product::where('id', $req->id)->delete();
    }

    public function delete_all(Request $req){
        can_delete();
        if ($req->id) {
            Product::whereIn('id', $req->id)->delete();
            return redirect(route('product.index'))->with('sukses', 'Data berhasil dihapus');
        } else {
            return redirect(route('product.index'))->with('gagal', 'Data gagal dihapus');
        }
    }

    public function daftar_product_json(Request $req){
        if($req->key){
        $datas = Product::with('kategori')->where('id_kategori', $req->key)->latest()->paginate(8);
        }else{
        $datas = Product::with('kategori')->latest()->paginate(8);
        }
    
        if($req->search){
            $datas = Product::with('kategori')->where('produk.deskripsi','like','%'.$req->search.'%')->orWhere('produk.harga','like','%'.$req->search.'%')->paginate(8); 
        }
        return response()->json($datas);
   }

   public function save(Request $req)
    {
        $validator = Validator::make($req->all(),

        array(
            'kategori'              => 'required',
            'nama'                  => 'required|unique:produk,nama', 
            'deskripsi'             => 'required',
            'harga'                 => 'required',
            'foto'                  => 'required'
        ),

        array(
            'kategori.required'             => 'kategori tidak boleh kosong',
            'nama.required'                 => 'nama produk tidak boleh kosong',
            'nama.unique'                   => 'nama produk telah digunakan',
            'deskripsi.required'            => 'deskripsi tidak boleh kosong',
            'harga.required'                => 'harga tidak boleh kosong',
            'foto.required'                 => 'foto tidak boleh kosong'
           
         )
    );

    //Kondisi validasi (Pesan Error yang akan terjadi)
    if ($validator->fails()) {
       return response()->json(array('errors' => $validator->errors()));
    }else{
         
        $foto = $req->file('foto');
        $fileName = $foto->getClientOriginalName();
        request()->foto->move(public_path('assets/images/product_images'), $fileName);

        $product                 = new Product;

        if ($req->id != null) {
         $product           = Product::find($req->id);
         $product->exists   = true;
        }
        
        $product->id_kategori    = $req->kategori;
        $product->nama           = $req->nama;
        $product->deskripsi      = $req->deskripsi;
        $product->harga          = preg_replace("/[^0-9]/", "",$req->harga);
        $product->foto           = $fileName;

        $save = $product->save();

        if($save){
            return response()->json(array('status' => true, 'message' => 'Data berhasil disimpan'));
        }else{
            return response()->json(array('status' => false, 'message' => 'Data gagal disimpan'));
        }
    }

    }


}
