<?php

namespace App\Http\Controllers\BackEnd\Module;

use App\Http\Controllers\BackEndController;
use App\Module;
use App\Users;
use App\Menu;
use DataTables;
use Illuminate\Http\Request;
use Validator;

class ModuleController extends BackEndController
{

    public function index()
    {
        $data['user'] = Users::with('permission_users')->get();
        return view('module.index', $data);
    }

    public function jsondata()
    {

        $data = Module::get();

        return Datatables::of($data)

            ->addColumn('x', function ($data) {
                return '<input name="id[]" value="' . $data->id . '" class="check-item" type="checkbox">';
            })

            ->addColumn('No', function ($data) {
                return '';
            })
            ->addColumn('name', function ($data) {
                return $data->name;
            })

            ->addColumn('slug', function ($data) {
                return $data->slug;
            })

            ->addColumn('description', function ($data) {
                return $data->description;
            })

            ->addColumn('active', function ($data) {
                if ($data->active == 1) {
                    $active = "Active";
                } else {
                    $active = "Not Active";
                }
                return $active;
            })

            ->addColumn('action', function ($data) {
                return '<a href="' . route('module.edit', $data->id) . '" class="btn btn-xs btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a> <a href="#" data-id="'.$data->id.'" class="btn delete_account btn-delete btn-sm btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->rawColumns(['x', 'action'])
            ->make(true);

    }

    public function add()
    {
        can_create();
        $data['menu'] = Menu::get();
        return view('module.form', $data);
    }

    public function save(Request $req)
    {

        $validator = Validator::make($req->all(),

            array(
                'name' => 'required',
                'slug' => 'required',
                'description' => 'required',
                'menu' => 'required|numeric'
            ),

            array(
                'name.required' => 'name tidak boleh kosong',
                'slug.required' => 'slug tidak boleh kosong',
                'description.required' => 'description tidak boleh kosong',
                'menu.required' => 'menu tidak boleh kosong',
                'menu.numeric' => 'menu harus dipilih'
            )
        );

        //Kondisi validasi (Pesan Error yang akan terjadi)
        if ($validator->fails()) {
            if($req->id!=null){
            return redirect(route('module.edit', $req->id))->withErrors($validator);
            }else{
            return redirect(route('module.add'))->withErrors($validator);
            }
        } else {
            $module = new Module;

            if ($req->active != null) {
                $active = '1';
            } else {
                $active = '0';
            }

            if ($req->id != null) {
                $module = Module::find($req->id);
                $module->exists = true;
                $module->active = $req->active;
            }
            $module->menu_id = $req->menu;
            $module->name = $req->name;
            $module->slug = $req->slug;
            $module->description = $req->description;
            $module->active = $active;

            $save = $module->save();
            if($save){
                return redirect(route('module.modules'))->with('sukses', 'Data berhasil tersimpan');
            }else{
                return redirect(route('module.modules'))->with('gagal', 'Data berhasil tersimpan');
            }
            

        }

    }

    public function delete(Request $req)
    {
        can_delete();
        Module::where('id', $req->id)->delete();
      
    }

    public function edit($id)
    {
        can_edit();
        $data['module'] = Module::find($id);
        $data['menu'] = Menu::get();
        return view('module.form', $data);
    }

    public function delete_all(Request $req)
    {
        can_delete();
        if ($req->id) {
            Module::whereIn('id', $req->id)->delete();
            return redirect(route('module.modules'))->with('sukses', 'Data berhasil dihapus');
        } else {
            return redirect(route('module.modules'))->with('gagal', 'Data gagal dihapus');
        }
    }

}
