<?php

namespace App\Http\Controllers\BackEnd\Role;

use App\Http\Controllers\BackEndController;
use App\Menu_role;
use App\Module;
use App\Permission_role;
use App\Role;
use DataTables;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator;

class RoleController extends BackEndController
{

    public function index()
    {
        return view('role.index');
    }

    public function add()
    {
        can_create();
        return view('role.form');
    }

    public function save(Request $req)
    {

        $validator = Validator::make($req->all(),

            array(
                'name' => 'required',
                'description' => 'required',
            ),

            array(
                'name.required' => 'username tidak boleh kosong',
                'description.required' => 'deskripsi tidak boleh kosong',
            )
        );

        //Kondisi validasi (Pesan Error yang akan terjadi)
        if ($validator->fails()) {
            if ($req->id != null) {
                return redirect(route('role.edit', $req->id))->withErrors($validator);
            } else {
                return redirect(route('role.add'))->withErrors($validator);
            }
        } else {

            $role = new Role;
            if ($req->id != null) {
                $role = Role::find($req->id);
                $role->exists = true;
            }
            $role->name = $req->name;
            $role->description = $req->description;
            $role = $role->save();
            if ($role) {
                return redirect(route('role.roles'))->with('sukses', 'Data berhasil tersimpan');
            } else {
                return redirect(route('role.roles'))->with('gagal', 'Data berhasil tersimpan');
            }

        }

    }

    public function delete(Request $req)
    {
        can_delete();
        $role = Role::where('id', $req->id)->delete();
    }

    public function jsondata()
    {

        $data = Role::get();
        return Datatables::of($data)

            ->addColumn('x', function ($data) {
                return '<input name="id[]" value="' . $data->id . '" class="check-item" type="checkbox">';
            })

            ->addColumn('No', function ($data) {
                return '';
            })
            ->addColumn('name', function ($data) {
                return $data->name;
            })
            ->addColumn('display_name', function ($data) {
                return $data->display_name;
            })
            ->addColumn('description', function ($data) {
                return $data->description;
            })
            ->addColumn('action', function ($data) {

                if ($data->id != 1) {
                    return '<a href="' . route('role.access', $data->id) . '" class="btn btn-xs btn-success btn-sm"><i class="fa fa-check"></i> Access</a>
              <a href="' . route('role.edit', $data->id) . '" class="btn btn-xs btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-delete btn-sm btn-xs btn-danger" data-id="' . $data->id . '"><i class="fa fa-trash"></i> Delete</a>';
                } else {
                    return '<a href="' . route('role.edit', $data->id) . '" class="btn btn-xs btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-delete btn-sm btn-xs btn-danger" data-id="' . $data->id . '"><i class="fa fa-trash"></i> Delete</a>';
                }

            })
            ->rawColumns(['x', 'action'])
            ->make(true);

    }

    public function access($id)
    {
        can_create();
        $data['module'] = Module::with('permission_roles')->where('module.active', 1)->orderBy('menu_id', 'ASC')->get();
        Session::put('role', $id);
        $data['role'] = Permission_role::where('role_id', $id)->first();
        $data['menu'] = Menu_role::where('role_id', $id)->first();
        return view('role.add_access', $data);
    }

    public function saveaccess(Request $req, $id)
    {

        $read = $req->input('read');
        $add = $req->input('add');
        $edit = $req->input('edit');
        $delete = $req->input('delete');
        $module = $req->input('id');
        $menu_id = $req->input('menu_id');

        DB::table('permission_role')->where('role_id', $id)->delete();
        foreach ($module as $v) {
            $data = array('module_id' => $module[$v],
                'role_id' => $id,
                'can_access' => isset($read[$v]),
                'can_create' => isset($add[$v]),
                'can_edit' => isset($edit[$v]),
                'can_delete' => isset($delete[$v]));

            try {

                $save_permisssion = DB::table('permission_role')->insert($data);
            } catch (Exception $e) {
                //return 'Duplicate Entry';
                return redirect(route('role.access', $id))->with('sukses', 'Data berhasil tersimpan');
            }

        }
        DB::table('menu_role')->where('role_id', $id)->delete();
        if ($req->input('menu_id')) {
            //dd($req->input('menu_id'));
            $menu_id = implode(',', $menu_id);
            $data = array('role_id' => $id,
                'menu_id' => $menu_id);
            DB::table('menu_role')->insert($data);
        }
        $req->session()->forget('role');
        return redirect(route('role.access', $id))->with('sukses', 'Data berhasil tersimpan');

    }

    public function edit($id)
    {
        can_edit();
        $data['role'] = Role::find($id);
        return view('role.form', $data);
    }

    public function delete_all(Request $req)
    {
        can_delete();
        if ($req->id) {
            $role = Role::whereIn('id', $req->id)->delete();
            return redirect(route('role.roles'))->with('sukses', 'Data berhasil dihapus');
        } else {
            return redirect(route('role.roles'))->with('gagal', 'Data gagal dihapus');
        }
    }

}
