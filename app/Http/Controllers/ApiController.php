<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Kategori;
use Validator;

class ApiController extends Controller
{
    public function get_all_kategori(Request $req){
        return response()->json(array('status'=>true,
                                      'data'=>Kategori::all()), 200);
    }

    public function get_all_product(Request $req){
        return response()->json(array('status'=>true,
                                      'data'=>Product::all()), 200);
    }

    public function save_kategori(Request $req){
        $validator = Validator::make($req->all(),

        array(
            'nama' => 'required'
        ),

        array(
            'nama.required' => 'nama kategori tidak boleh kosong'
        )
    );

    //Kondisi validasi (Pesan Error yang akan terjadi)
    if ($validator->fails()) {
        return response()->json(array('status'=>false,
                                      'message'=>'Data gagal disimpan',
                                      'data'=>$validator->errors()), 200);
    }else{
        $kategori = New Kategori;
        $kategori->nama = $req->nama;
        $save = $kategori->save();
        if($save){
            return response()->json(array('status'=>true,
                              'message'=>'Data berhasil disimpan',
                              'data'=>$kategori), 200); 
        }else{
            return response()->json(array('status' => false, 'message' => 'Data gagal disimpan'));
        }
    }

    }

    public function save_product(Request $req){
        $validator = Validator::make($req->all(),

        array(
            'kategori'              => 'required',
            'nama'                  => 'required|unique:produk,nama', 
            'deskripsi'             => 'required',
            'harga'                 => 'required',
            'foto'                  => 'required'
        ),

        array(
            'kategori.required'             => 'kategori tidak boleh kosong',
            'nama.required'                 => 'nama produk tidak boleh kosong',
            'nama.unique'                   => 'nama produk telah digunakan',
            'deskripsi.required'            => 'deskripsi tidak boleh kosong',
            'harga.required'                => 'harga tidak boleh kosong',
            'foto.required'                 => 'foto tidak boleh kosong'
           
         )
    );

    //Kondisi validasi (Pesan Error yang akan terjadi)
    if ($validator->fails()) {
         return response()->json(array('status'=>false,
                              'message'=>'Data gagal disimpan',
                              'data'=>$validator->errors()), 200);
    }else{
         
        $foto = $req->file('foto');
        $fileName = $foto->getClientOriginalName();
        request()->foto->move(public_path('assets/images/product_images'), $fileName);

        $product                 = new Product;

              
        $product->id_kategori    = $req->kategori;
        $product->nama           = $req->nama;
        $product->deskripsi      = $req->deskripsi;
        $product->harga          = preg_replace("/[^0-9]/", "",$req->harga);
        $product->foto           = $fileName;

        $save = $product->save();

        if($save){
            return response()->json(array('status'=>true,
                              'message'=>'Data berhasil disimpan',
                              'data'=>$product), 200); 
        }else{
            return response()->json(array('status' => false, 'message' => 'Data gagal disimpan'));
        }
    }

    }

    public function put_product(Request $req){

        $product                 = new Product;

        if ($req->id != null) {
         $product           = Product::find($req->id);
         $product->exists   = true;
        }

        $foto = $req->file('foto');
        $fileName = $foto->getClientOriginalName();
        request()->foto->move(public_path('assets/images/product_images'), $fileName);

        $product->id_kategori    = $req->kategori;
        $product->nama           = $req->nama;
        $product->deskripsi      = $req->deskripsi;
        $product->harga          = preg_replace("/[^0-9]/", "",$req->harga);
        $product->foto           = $fileName;

        $save = $product->save();

        if($save){
            return response()->json(array('status'=>true,
                              'message'=>'Data berhasil diupdate',
                              'data'=>$product), 200); 
        }else{
            return response()->json(array('status' => false, 'message' => 'Data gagal diupdate'));
        }
    }

    public function delete_product(Request $req){
        $delete = Product::destroy($req->id);
        if($delete){
            return response()->json(array('status'=>true,
                                    'message'=>'Data berhasil dihapus'
                                     ), 200); 
        }else{
            return response()->json(array('status' => false, 'message' => 'Data gagal dihapus'));
        }
    }

    public function put_kategori(Request $req){
         $kategori = new Kategori;
         if ($req->id != null) {
         $kategori           = Kategori::find($req->id);
         $kategori->exists   = true;
        }

        $kategori->nama = $req->nama;
        $save = $kategori->save();
        if($save){
            return response()->json(array('status'=>true,
                              'message'=>'Data berhasil diupdate',
                              'data'=>$kategori), 200); 
        }else{
            return response()->json(array('status' => false, 'message' => 'Data gagal diupdate'));
        }

    }

    public function delete_kategori(Request $req){
        $delete = Kategori::destroy($req->id);
        if($delete){
            return response()->json(array('status'=>true,
                                    'message'=>'Data berhasil dihapus'
                                     ), 200); 
        }else{
            return response()->json(array('status' => false, 'message' => 'Data gagal dihapus'));
        }
    }
}
