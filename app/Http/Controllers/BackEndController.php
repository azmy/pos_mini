<?php

namespace App\Http\Controllers;

use App\Module;
use App\Permission_role;
use Auth;
use Illuminate\Http\Request;

class BackEndController extends Controller
{

    public function __construct(Request $request)
    {

        $this->middleware('auth');
        $this->middleware(function ($request, $next) {

            if (@Auth::user()->role_id == 1) {
                return $next($request);
            } else {
               /*  $permission_role = Permission_role::where('role_id', @Auth::user()->role_id)->get(); */
                $slug = array();
                $acces = array();
                $current_url = request()->segment(1) . "/" . request()->segment(2);
                if ($current_url == "admin/" or request()->ajax()) {
                    return $next($request);
                } else {
                    $permission = Module::where(array('slug' => $current_url, 'active' => 1))->first();
                    $module = Permission_role::where(array('module_id' => @$permission->id, 'can_access' => 1, 'role_id' => @Auth::user()->role_id))->first();
                    $slug[] = @$permission->slug;
                    $acces[] = @$module->can_access;
                    $can_acces = 1;
                    if (in_array($can_acces, $acces, true)) {
                        return $next($request);
                    } else {
                        redirect()->to('admin/404')->send();
                    }

                }

            }
        });
    }

}
