<?php

namespace App\Http\Controllers\FrontEnd\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\FrontEndController;
use App\Kategori;
use App\Product;

class HomeController extends FrontEndController
{
    public function index(){
        $data['kategori'] = Kategori::get();
        return view('daftar_product.daftar_product', $data);
    }

    public function daftar_product_json(Request $req){
        if($req->key){
        $datas = Product::with('kategori')->where('id_kategori', $req->key)->latest()->paginate(8);
        }else{
        $datas = Product::with('kategori')->latest()->paginate(8);
        }
    
        if($req->search){
            $datas = Product::with('kategori')->where('produk.nama','like','%'.$req->search.'%')->orWhere('produk.deskripsi','like','%'.$req->search.'%')->orWhere('produk.harga','like','%'.$req->search.'%')->paginate(8); 
        }
        return response()->json($datas);
   }

}
