<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (Request::is('admin/*') or Request::is('admin'))
{
    
    require __DIR__.'/backend.php'; 
}

if (Request::is('/*') or Request::is('front'))
{
    
    require __DIR__.'/frontend.php'; 
}





Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
