<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/kategori', 'ApiController@get_all_kategori');
Route::post('/kategori/post', 'ApiController@save_kategori');
Route::put('/kategori/put', 'ApiController@put_kategori');
Route::delete('/kategori/delete/{id?}', 'ApiController@delete_kategori');

Route::get('/produk', 'ApiController@get_all_product');
Route::post('/produk/post', 'ApiController@save_product');
Route::put('/produk/put', 'ApiController@put_product');
Route::delete('/produk/delete/{id?}', 'ApiController@delete_product');