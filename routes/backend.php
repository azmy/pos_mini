<?php

Route::group(['middleware' => 'auth'], function() {
Route::get('/', 'Dashboard\HomeController@index')->name('/');

Route::get('/gantipwd', 'GantiPassword\GantiPasswordController@index')->name('gantipassword.index');
Route::post('gantipwd/save', 'GantiPassword\GantiPasswordController@save')->name('gantipassword.save');
});

Route::get('/user', 'Users\UserController@index')->name('user.users');
Route::get('user/add', 'Users\UserController@add')->name('user.add');
Route::post('user/save/{id?}', 'Users\UserController@save')->name('user.save');
Route::get('user/delete/{id?}', 'Users\UserController@delete')->name('user.delete');
Route::post('user/delete_all', 'Users\UserController@delete_all')->name('user.delete_all');
Route::get('user/edit/{id?}', 'Users\UserController@edit')->name('user.edit');
Route::get('/userjson', 'Users\UserController@jsondata')->name('users.jsondata');

Route::get('/role', 'Role\RoleController@index')->name('role.roles');
Route::get('role/add', 'Role\RoleController@add')->name('role.add');
Route::post('role/save/{id?}', 'Role\RoleController@save')->name('role.save');
Route::post('role/saveaccess/{id}', 'Role\RoleController@saveaccess')->name('role.saveaccess');
Route::delete('role/delete/{id?}', 'Role\RoleController@delete')->name('role.delete');
Route::post('/role/delete_all', 'Role\RoleController@delete_all')->name('role.delete_all');
Route::get('role/edit/{id}', 'Role\RoleController@edit')->name('role.edit');
Route::get('role/access/{id}', 'Role\RoleController@access')->name('role.access');
Route::get('/rolejson', 'Role\RoleController@jsondata')->name('role.jsondata');

Route::get('/acl', 'Acl\AclController@index')->name('acl.acl');
Route::get('/module', 'Module\ModuleController@index')->name('module.modules');
Route::get('module/add', 'Module\ModuleController@add')->name('module.add');
Route::post('module/save/{id?}', 'Module\ModuleController@save')->name('module.save');
Route::delete('module/delete/{id?}', 'Module\ModuleController@delete')->name('module.delete');
Route::post('module/delete_all', 'Module\ModuleController@delete_all')->name('module.delete_all');
Route::get('module/edit/{id?}', 'Module\ModuleController@edit')->name('module.edit');
Route::get('/modulejson', 'Module\ModuleController@jsondata')->name('module.jsondata');

Route::get('/menu', 'Menu\MenuController@index')->name('menu.menus');
Route::get('menu/add', 'Menu\MenuController@add')->name('menu.add');
Route::post('menu/save/{id?}', 'Menu\MenuController@save')->name('menu.save');
Route::delete('menu/delete/{id?}', 'Menu\MenuController@delete')->name('menu.delete');
Route::post('/menu/delete_all/{id?}', 'Menu\MenuController@delete_all')->name('menu.delete_all');
Route::get('/menu/edit/{id?}', 'Menu\MenuController@edit')->name('menu.edit');
Route::get('/menujson', 'Menu\MenuController@jsondata')->name('menu.jsondata');
Route::get('/menu/slug/{id?}', 'Menu\MenuController@search_slug')->name('menu.searchslug');

Route::get('/kategori', 'Kategori\KategoriController@index')->name('kategori.index');
Route::get('/kategori/jsondata', 'Kategori\KategoriController@jsondata')->name('kategori.jsondata');
Route::get('/kategori/add', 'Kategori\KategoriController@add')->name('kategori.add');
Route::get('/kategori/edit/{id?}', 'Kategori\KategoriController@edit')->name('kategori.edit');
Route::post('/kategori/save/{id?}', 'Kategori\KategoriController@save')->name('kategori.save');
Route::delete('/kategori/delete/{id?}', 'Kategori\KategoriController@delete')->name('kategori.delete');
Route::post('/kategori/delete_all/{id?}', 'Kategori\KategoriController@delete_all')->name('kategori.delete_all');

Route::get('/product', 'Product\ProductController@index')->name('product.index');
Route::get('/product/jsondata', 'Product\ProductController@jsondata')->name('product.jsondata');
Route::get('/product/add', 'Product\ProductController@add')->name('product.add');
Route::get('/product/edit/{id?}', 'Product\ProductController@edit')->name('product.edit');
Route::post('/product/save', 'Product\ProductController@save')->name('product.save');
Route::get('/daftar_product', 'Product\ProductController@daftar_product')->name('product.daftar_product');
Route::get('/daftar_product/json', 'Product\ProductController@daftar_product_json')->name('product.daftar_product_json');
Route::delete('/product/delete/{id?}', 'Product\ProductController@delete')->name('product.delete');
Route::post('/product/delete_all/{id?}', 'Product\ProductController@delete_all')->name('product.delete_all');



Route::get('/404', 'AccesDenied\AccesDeniedController@index')->name('404.404');



