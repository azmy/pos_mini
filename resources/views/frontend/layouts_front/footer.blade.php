 <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          <footer class="footer text-center" style="background-color:white;border:1px solid #e4e9f0;">
            <div class="container text-center">
              
            
                <span class="text-muted  d-block d-sm-inline-block">Copyright © 2019 <a href="#" target="_blank">PT. Majoo Teknologi Indonesia</a></span>
              
            
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>

 <!-- page-body-wrapper ends -->
 </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('assets/js/jquery.3.2.1.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    
   
   

    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
     

    <script type="text/javascript" src="{{asset('assets/js/jquery.twbsPagination.min.js')}}"></script>
   
    <script src="{{asset('assets/js/hoverable-collapse.js')}}"></script>
    
    <script src="{{asset('assets/js/settings.js')}}"></script>
    

    @yield('footer_scripts')
  </body>
</html>