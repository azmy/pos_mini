<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Pos Mini PT. Majoo Teknologi</title>
    
    
    <link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('assets/css/demo_2/style.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/demo_3/style.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/demo_2/custom.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}" />
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
    <script src="{{asset('assets/js/jquery.3.3.1.min.js')}}"></script>
  </head>
  
  @yield('styles')
  <body>
  
  
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Majoo Teknologi Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  
</nav>