@extends('template')
@section('main')
<style>
    .has-error{
    padding-top:10px;
    font-size:9px !important;
    }
</style>
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Master</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Form Pengguna</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> @if(!empty(@$customer->id)) Edit @else Add @endif Pengguna</li>
                    </ol>
                </nav>
            </div>



            <div class="row">
@if(Session::has('gagal'))
<div class="alert alert-danger">
    {{Session::get('gagal')}}
</div>
@endif

@if(Session::has('sukses'))
<div class="alert alert-success">
    {{Session::get('sukses')}}
</div>
@endif     
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Form Pengguna</h4>
                            <p class="card-description">Digunakan untuk menambah atau mengedit pengguna</p>
                            <form method="POST" action="{{route('user.save', @$user->id)}}" class="forms-sample">
                                @csrf
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$user->name}}" name="name" class="form-control" id="name"
                                        placeholder="Nama">
                                    </div>
                                    @error('name')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$user->username}}" name="username" class="form-control" id="username"
                                        placeholder="Username">
                                    </div>
                                    @error('username')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="hp">Email</label>
                                    <div class="input-group mb-3">
                                    <input type="email" value="{{@$user->email}}" name="email" class="form-control" id="email"
                                        placeholder="Email">
                                    </div>
                                    @error('email')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                @if(@$user->id!=NULL)
                                <div class="form-group">
                                <div class="form-check mx-sm-2">
                                <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="ganti_pass" value="1">
                                Ganti Password
                               </label>
                                </div>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <div class="input-group mb-3">
                                    <input type="password"  name="password" class="form-control" id="password"
                                        placeholder="Password">
                                    </div>
                                    @error('password')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="konfir_password">Konfirmasi Password</label>
                                    <div class="input-group mb-3">
                                    <input type="password"  name="konf_password" class="form-control" id="konf_password"
                                        placeholder="Konfirmasi Password">
                                    </div>
                                    @error('konf_password')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>  

                                <div class="form-group">
                                    <label for="password">Level Pengguna</label>
                                    <div class="input-group mb-3">
                                    <select class="form-control" name="level">
                                    <option>Pilih</option>
                                    @foreach($role as $roles)
                                    {{$sel = $roles->id == @$user->role_id ? 'selected':''}}
                                    <option value="{{$roles->id}}" {{$sel}}>{{@$roles->name}}</option>
                                     @endforeach
                                    </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                <div class="form-check mx-sm-2" style="margin-left:20px !important;font-size:11pt;">
                                <input type="checkbox" class="form-check-input" value="{{@$user->active}}" name="active" {{(@$user->active==1) ? 'checked':''}}> Active
                                </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary mr-2"> Simpan </button>
                                <a href="{{route('user.users')}}" class="btn btn-light">Batal</a>
                        </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>


        
        @endsection

    @section('footer_scripts')
      <script>
     
    </script>
    @endsection