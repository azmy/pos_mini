@extends('template')
@section('main')
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Pengguna</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> List Pengguna</li>
                    </ol>
                </nav>
            </div>

            <div class="page-header flex-wrap">
                <div class="header-left">
                    <!-- <button class="btn btn-primary mb-2 mb-md-0 mr-2"> Create new document </button> -->
                    <button class="btn btn-outline-primary mb-2 mb-md-0" style="display:none"> Import documents
                    </button>
                </div>
                <div class="header-right d-flex flex-wrap mt-md-2 mt-lg-0 pull-right">
                    <button type="button" class="btn btn-primary mt-2 mt-sm-0 btn-icon-text">
                        <i class="mdi mdi-plus-circle"></i><a href="{{route('user.add')}}" style="color:white;"> Add
                            User </a>
                    </button>
                   

                </div>

            
            </div>

            <div class="row">
            @if(Session::has('gagal'))
            <div class="alert alert-danger">
                {{Session::get('gagal')}}
            </div>
            @endif

            @if(Session::has('sukses'))
            <div class="alert alert-success">
                {{Session::get('sukses')}}
            </div>
            @endif     
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                            <form method="post" action="{{route('user.delete_all')}}" id="form-delete">
                            @csrf
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                            <th width="8px;"><input id="check-all" type="checkbox"></th>
                            <th width="8px;" >No</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Group</th>
                            <th style="width:10px;">Active</th>
                            <th style="width:12px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            </table>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
.toolbar {
    float:left;
}
.sorting_disabled
{
   background-image:none !important;
}
th.th-td-center, td.th-td-center { text-align: center; }
th.th-center { text-align: center; }
</style>
@endsection       

@section('footer_scripts')
<script>
  $(document).ready(function(){
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
  });
  var table = $('#datatable-responsive').DataTable({
        dom: '<"toolbar"><l>Bfrtip',
        processing: true,
        serverSide: true,
        destroy: true,
        ordering: false,
        initComplete:   function()  //Adding Custom button in Tools
        {
            $("div.toolbar").html('<button type="button" style="float:left;margin:20px;" onclick="delete_all()" class="btn btn-danger btn-rounded btn-icon mt-2 mt-sm-0 btn-icon-text"><i class="mdi mdi-close-circle"></i></button>');
        },
        ajax: '{{route('users.jsondata')}}',
        columns:[ 
            {data:'x', name:'Check All',orderable: false},               
            {data:'No', name:'No'},
            {data: 'name', name:'name'},
            {data: 'username', name:'username'},
            {data: 'email', name:'email'},
            {data: 'group_name', name:'group_name'},
            {data: 'active', name:'active',className: 'th-td-center'},
            {data: 'action', name: 'action',className: 'th-td-center'}
        ],
       
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
         var index = iDisplayIndex +1;
        $('td:eq(1)',nRow).html(index);
        return nRow;
       },

       columnDefs: [{
        targets: [0],
        orderable: false
    }]

    });

    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var href=$(this).attr('href');

        swal({
            title: "Anda Yakin?",
            text: "Sistem akan menghapus data ini!",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yakin, hapus saja!",
            cancelButtonText: "Tidak, batalkan!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href=href;
            }
        }); 
    });
    
    });

    //Checkbox all
    $(document).ready(function(){ 
    $("#check-all").click(function(){ 
      if($(this).is(":checked")) 
        $(".check-item").prop("checked", true); 
      else
        $(".check-item").prop("checked", false); 
    });
    
    
     

  });

  function delete_all() {
        var table = $('#basic-datatables').DataTable();
        swal({
            title: 'Anda Yakin ?',
            text: "Sistem Akan Menghapus data ini !",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Ya',
                    className: 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                table.ajax.reload();
                $("#form-delete").submit(); // Submit form
                return true;

            } else {
                swal.close();
                $(this).find('button[type="submit"]').prop('disabled', true);
                return false;
            }
        });
    }
</script>
@endsection


