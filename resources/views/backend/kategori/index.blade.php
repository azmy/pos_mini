@extends('template')
@section('main')
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Kategori</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Kategori</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> List Kategori </li>
                    </ol>
                </nav>
            </div>

            

            <div class="row">
                @if(Session::has('gagal'))
                <script type="text/javascript">
                $(document).ready(function () {
                $(".alert-danger").delay(2400).fadeOut(2400);
                });
                </script>
                <div class="alert alert-danger">
                    {{Session::get('gagal')}}
                </div>
                @endif

                @if(Session::has('sukses'))
                <script type="text/javascript">
                $(document).ready(function () {
                $(".alert-success").delay(2400).fadeOut(2400);
                });
                </script>
                <div class="alert alert-success">
                    {{Session::get('sukses')}}
                </div>
                @endif 
                  
                <div class="col-lg-12 grid-margin stretch-card">
                
                    <div class="card">
                        <div class="card-header">
                        <div class="header-right d-flex flex-wrap mt-md-2 mt-lg-0 pull-right">
                         <button type="button" class="btn btn-primary mt-2 mt-sm-0 btn-icon-text">
                            <i class="mdi mdi-plus-circle"></i><a href="{{route('kategori.add')}}" style="color:white"> Add
                            Kategori </a>
                            </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <form method="post" action="{{route('kategori.delete_all')}}" id="form-delete">
                            @csrf
                                <table class="table table-striped" id="basic-datatables">
                                    <thead>
                                        <tr>
                                           <th width="4px;" class="order">
                                               <input id="check-all" type="checkbox" class="form-check form-check-primary">
                                            </th>
                                            <th width="10px;" >No</th>
                                            <th>Kategori</th>
                                            <th width="40px;" >#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection
<style>
    .btn-primary{
        background:#0033c4 !important;
        border-radius:4px !important;
    }
</style>
@section('footer_scripts')
<script>
$(document).ready(function() {
  
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    var table = $('#basic-datatables').DataTable({
        dom: '<"toolbar"><l>Bfrtip',
        processing: true,
        serverSide: true,
        ordering: true,
        destroy: true,
        initComplete:   function()  //Adding Custom button in Tools
        {
        $("div.toolbar").html('<button type="button" style="float:left;margin:20px;" onclick="delete_all()" class="btn btn-danger btn-rounded btn-icon mt-2 mt-sm-0 btn-icon-text"><i class="mdi mdi-close-circle"></i></button>');
        },
        ajax: '{{route('kategori.jsondata')}}',
        columns:[                
            {data:'x', name:'Check All',className: "order",orderable: false},  
            {data:'no', name:'no',className: "th-center"},
            {data: 'kategori', name:'kategori',className: "th-center"},
            {data: 'action', name: 'action',className: "th-td-center"}
        ],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
         var index = iDisplayIndex +1;
        $('td:eq(1)',nRow).html(index);
        return nRow;
       }
    });
});

$(document).on('click', '.btn-delete', function(e) {
        e.preventDefault();
        var table = $('#basic-datatables').DataTable();
        var href = $(this).attr('href');
        var id = $(this).data('id');
        swal({
            title: 'Anda Yakin ?',
            text: "Sistem Akan Menghapus data ini !",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Ya',
                    className: 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                $.ajax({
                    type: "delete",
                    url: "{{route('kategori.delete')}}",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        table.ajax.reload();
                        swal('Data berhasil dihapus !', {
                            buttons: {
                                confirm: {
                                    className: 'btn btn-success'
                                }
                            },
                        });
                    }
                });
            } else {
                swal.close();
            }
        });

    });


    function delete_all() {
        var table = $('#basic-datatables').DataTable();
        swal({
            title: 'Anda Yakin ?',
            text: "Sistem Akan Menghapus data ini !",
            type: 'warning',
            buttons: {
                confirm: {
                    text: 'Ya',
                    className: 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                table.ajax.reload();
                $("#form-delete").submit(); // Submit form
                return true;

            } else {
                swal.close();
                $(this).find('button[type="submit"]').prop('disabled', true);
                return false;
            }
        });
    }

    //Checkbox all
    $(document).ready(function() {
        $("#check-all").click(function() {
            if ($(this).is(":checked"))
                $(".check-item").prop("checked", true);
            else
                $(".check-item").prop("checked", false);
        });
    });

</script>
@endsection