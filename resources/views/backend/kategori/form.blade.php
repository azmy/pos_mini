@extends('template')
@section('main')
<style>
    .has-error{
    padding-top:10px;
    font-size:9px !important;
    }
</style>
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Form Kategori</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Kategori</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> @if(!empty(@$kategori->id)) Edit @else Add @endif Kategori </li>
                    </ol>
                </nav>
            </div>



            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Form Kategori</h4>
                            <p class="card-description">Digunakan untuk menambah atau mengedit kategori</p>
                            <form method="POST" action="{{route('kategori.save', @$kategori->id)}}" class="forms-sample">
                                @csrf
                                <div class="form-group">
                                    <label for="kategori">Kategori</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$kategori->nama}}" name="kategori" class="form-control" id="kategori"
                                        placeholder="Kategori">
                                    </div>
                                    @error('kategori')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>
                                
                                <button type="submit" class="btn btn-primary mr-2"> Simpan </button>
                                <a href="{{route('kategori.index')}}" class="btn btn-light">Batal</a>
                        </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>


        
        @endsection

    @section('footer_scripts')
      <script>
     
    </script>
    @endsection