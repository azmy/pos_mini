@extends('template')
@section('main')
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Daftar Produk</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Produk</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> Daftar Produk</li>
                    </ol>
                </nav>
            </div>

            
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        <h5 class="card-title">Data Product</h5>
                        <div class="row">
                            <div class="col-sm-6">
                             <select class="form-control" id="cat">
                              <option value="" selected>Pilih Kategori</option>
                              @foreach($kategori as $kategori)
                             <option value="{{$kategori->id}}">{{$kategori->nama}}</option>
                              @endforeach
                             
                             </select>
                            </div>
                            <div class="col-sm-6 mb-3">
                              
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="" name="cari" id="cari" autocomplete="off" placeholder="Cari Produk">
                                        <div class="input-group-append">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary btn-md" id="btn-cari">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="#" class="btn btn-success btn-md btn-block btn-refresh">
                                                <i class="fa fa-refresh"></i> 
                                            </a>
                                        </div>
                                    </div>
                              
                            </div>
                        </div>
                        <div class="row product-item-wrapper" id="load_menu">
                        </div>
                        <br>
                        <div class="row">
                        <ul id="pagination" class="pagination-sm" style="padding:10px;overflow-y:hidden; overflow-x: hidden;"></ul>
                        </div>
                 </div>
                </div>
                </div>
               

            </div>
        </div>
    </div>
</div>
<style>
    
</style>



@endsection

@section('footer_scripts')

<script>





$(document).ready(function() {
  
var page = 1;
var current_page = 1;
var total_page = 0;
var is_ajax_fire = 0;
 
 
manageData();
 
 
/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: '{{route("product.daftar_product_json")}}',
        data: {page:page},
    beforeSend: function(){
        $("#load_menu").html('<center><img id="loader" src="{{asset("assets/images/animate")}}/loading.gif"></center>');
    },
    }).done(function(data){
 
       
        total_page = data.last_page+1;
        current_page = data.current_page;
 
 
        $('#pagination').twbsPagination({
            totalPages: total_page,
            visiblePages: current_page,
            onPageClick: function (event, pageL) {
                page = pageL;
             
                if(is_ajax_fire != 0){
                  getPageData();
                }
            }
        });
 
 
        manageRow(data.data);
        is_ajax_fire = 1;
    });
}
 
 

 
 
/* Get Page Data*/
function getPageData() {
    var search  = $("#cari").val();
    var key     = $("#cat").val();
    $.ajax({
        dataType: 'json',
        url: '{{route("product.daftar_product_json")}}',
        data: {page:page,key:key,search:search},
    beforeSend: function(){
        $("#load_menu").html('<center><img id="loader" src="{{asset("assets/images/animate")}}/loading.gif"></center>');
    },
    }).done(function(data){
        manageRow(data.data);
    });
}
 
const rupiah = (number)=>{
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR"
    }).format(number);
  }
 
/* Add new Item table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        if(value.foto!=null){
            var foto = value.foto;
        }else{
            var foto = 'no-image.png';
        }
        rows = rows + '<div class="col-lg-3 col-md-6 col-sm-6 col-12 product-item"  data-id="'+value.id+'" data-kategori="'+value.kategori.nama+'" data-nama="'+value.nama+'" data-harga="'+value.harga+'">';
        rows = rows + '<div class="card"><div class="card-body text-center"><div class="product-img-outer">';
        rows = rows + '<img width="100px;" height="200px;" class="product_image" src="{{asset("assets/images/product_images/")}}/'+foto+'" alt="prduct image"><br><br>';
        rows = rows + '<span>'+value.nama+'</span><br>';
        rows = rows +  '<b class="text-center">'+rupiah(value.harga)+'<input type="hidden" id="harga" value="'+value.harga+'"></b>';            
        rows = rows +  '<b class="text-center">'+value.deskripsi+'</b><br>';
        rows = rows + '</div> </div> </div></div>';
       
    });
 
 
    $("#load_menu").html(rows);
}




$('#cat').on('change', function (e) {
    getPageData();
});

$(document).on('click','#btn-cari',function (e) {
    getPageData();
});
$('#cari').keypress(function(event){
    if(event.keyCode == 13){
     $('#btn-cari').click();
    }
});

$(document).on('click','.btn-refresh',function (e) {
    e.preventDefault();
    getPageData();
});






});






   

   
    
</script>
@endsection