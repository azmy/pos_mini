@extends('template')
@section('main')
<style>
    .has-error{
    padding-top:10px;
    font-size:9px !important;
    }
</style>
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Form Menu</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Menu</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> @if(!empty(@$menu->id)) Edit @else Add @endif Menu </li>
                    </ol>
                </nav>
            </div>



            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Form Product</h4>
                            <p class="card-description">Digunakan untuk menambah atau mengedit product</p>
                            <form  id="formproduct" class="forms-sample" enctype="multipart/form-data">
                                @csrf
                               

                                <div class="form-group">
                                    <label for="kategori">Kategori</label>
                                    <div class="input-group mb-3">
                                    <select name="kategori" class="form-control kategori" style="width: 100%;padding:10px !important;">
                                    <option value="">Pilih Kategori</option>
                                    @foreach($kategori as $kategori)
                                    {{$sel = $kategori->id == @$product->id_kategori ? 'selected':''}}
                                    <option value="{{$kategori->id}}" {{$sel}}>{{$kategori->nama}}</option>
                                    @endforeach
                                    </select> 
                                    
                                    </div>
                                    <span id="kategori_error" class="error_validate"></span> 
                                </div>

                                <div class="form-group">
                                    <label for="nama">Nama Product</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$product->nama}}" name="nama" class="form-control" id="nama"
                                        placeholder="Nama Product">
                                        
                                    </div>
                                    <span id="nama_error" class="has-error has-feedback error text-danger"></span>
                                                              
                                </div>

                                <div class="form-group">
                                    <label for="deskripsi">Deskripsi</label>
                                    <div class="input-group mb-3">
                                    <textarea class="form-control my-editor" name="deskripsi" id="deskripsi" placeholder="Masukkan Deskripsi" required>{{@$product->deskripsi}}</textarea>
                                    <span id="deskripsi_error" class="error_validate"></span>
                                                                    
                                </div>

                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@format_uang($product->harga)}}" autocomplete="off" name="harga" class="form-control" id="harga"
                                        placeholder="Harga">
                                    </div>
                                    <span id="harga_error"></span>
                                   
                                </div>


                                <div class="form-group">
                                    <label for="foto">Foto</label>
                                    <div class="input-group mb-3">
                                    <input type="file" name="foto" class="form-control">
                                    </div>
                                    <span id="foto_error"></span>
                                    
                                    <div class="progress">
                                        <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div >
                                        <div class="percent">0%</div >
                                    </div>

                                </div>

                                


                                
                                <button type="button" id="saveproduct" class="btn btn-primary mr-2"> Simpan </button>
                                <a href="{{route('product.index')}}" class="btn btn-light">Batal</a>
                      
                        </form>
                    </div>
                </div>
                </div>
                </div>

            
      
    

         
      

<style>
  .tox-notification { display: none !important }
  .tox-tinymce{ width:100% !important;}
  .progress { position:relative; width:100%;height:20px;; border: 1px solid #ddd; padding: 
    1px; border-radius: 3px; }

.percent { position:absolute; display:inline-block; top:3px; left:48%; }
</style>
        
  @endsection

@section('footer_scripts')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<link href="{{asset('assets/css/select2/select2.min.css')}}" rel="stylesheet"/>
<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
<script>
 tinymce.init({
      selector: 'textarea.my-editor',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      height: 300,
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help'
           
   });

$(".kategori").select2({
        placeholder: "Pilih Kategori"
});
$(document).ready(function(){
   $('.progress').hide();
})
$('#saveproduct').on('click', function() {
  
    var bar             = $('.bar');
    var percent         = $('.percent');
    var deskripsi       = window.parent.tinymce.get('deskripsi').getContent();
    $('#formproduct').ajaxForm({
        url: '{{route('product.save')}}',
        type: 'post',
        data: {deskripsi: deskripsi},
        beforeSubmit: function(e) {
            console.log('before');
            $('.progress').show();
            $("#saveproduct").html("<i class='fa fa-spinner fa-spin'></i> Data Sedang Diproses...");
        },
    uploadProgress: function(event, position, total, percentComplete) {
    var percentVal = percentComplete+'%';
    bar.width(percentVal)
    percent.html(percentVal);

    var percentage = percentComplete;
    $('.progress .bar').css("width", percentage+'%', function() {
    return $(this).attr("aria-valuenow", percentage) + "%";
    })
    },
        success: function(data, status, xhr, form) {
            console.log('success');
            if(data.errors){
                if(data.errors.kategori){
                    $("#kategori_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.kategori[0]+'</label></span>');
                }
                if(data.errors.nama){
                    $("#nama_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.nama[0]+'</label></span>');
                }
                if(data.errors.deskripsi){
                    $("#deskripsi_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.deskripsi[0]+'</label></span>');
                }
                if(data.errors.harga){
                    $("#harga_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.harga[0]+'</label></span>');
                }
                if(data.errors.foto){
                    $("#foto_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.foto[0]+'</label></span>');
                }
            }else if(data.status===true){
                swal(data.message, {
                            buttons: {
                                confirm: {
                                    className: 'btn btn-success'
                                }
                            },
                });

                clear_error_validate();
            }
        },
        error: function(e) {
            console.log('error');
        },
        complete: function (xhr) {
         $('.progress').hide();
         $('#formproduct')[0].reset();
         window.parent.tinymce.get('deskripsi').setContent('');
         $("#saveproduct").html("Simpan");
        },
    }).submit();
});

function clear_error_validate(){
$("#kategori_error").html('');
$("#nama_error").html('');
$("#deskripsi_error").html('');
$("#harga_error").html('');
$("#foto_error").html('');
}
// $(document).on("click", "#saveproduct", function (event) {

// var bar = $('.bar');
// var percent = $('.percent');

// $('#formproduct').ajaxForm({
// beforeSend: function() {
//     //status.empty();
//     var percentVal = '0%';
//     bar.width(percentVal)
//     percent.html(percentVal);
// },
// success: function(data){
//     if(data.errors){
//                 if(data.errors.kategori){
//                     $("#kategori_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.kategori[0]+'</label></span>');
//                 }
//                 if(data.errors.nama){
//                     $("#nama_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.nama[0]+'</label></span>');
//                 }
//                 if(data.errors.harga){
//                     $("#harga_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.harga[0]+'</label></span>');
//                 }
//                 if(data.errors.foto){
//                     $("#foto_error").html('<span class="has-error has-feedback" role="alert"><label for="errorInput">'+data.errors.foto[0]+'</label></span>');
//                 }
//     }
// },
// uploadProgress: function(event, position, total, percentComplete) {
//     var percentVal = percentComplete+'%';
//     bar.width(percentVal)
//     percent.html(percentVal);

//     var percentage = percentComplete;
//     $('.progress .bar').css("width", percentage+'%', function() {
//     return $(this).attr("aria-valuenow", percentage) + "%";
//     })
// },
// complete: function(xhr) {
//  bar.width("100%");
//  percent.html("100%");
//  document.getElementById("formproduct").reset();
 
//     //status.html(xhr.responseText);
// }
// }); 
// }); 
      
    

    var harga_pokok = document.getElementById('harga');
    harga_pokok.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			harga_pokok.value = formatRupiah(this.value, '');
	});
   
    function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}
    </script>
    @endsection