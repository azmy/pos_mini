@extends('template')
@section('main')
<style>
    .has-error{
    padding-top:10px;
    font-size:9px !important;
    }
</style>
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Ganti Password</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">User</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ganti Password</li>
                    </ol>
                </nav>
            </div>



            <div class="row">
@if(Session::has('gagal'))
<div class="alert alert-danger">
    {{Session::get('gagal')}}
</div>
@endif

@if(Session::has('sukses'))
<div class="alert alert-success">
    {{Session::get('sukses')}}
</div>
@endif     
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Form Ganti Password</h4>
                            <p class="card-description">Digunakan untuk mengganti password pengguna</p>
                            <form method="POST" action="{{route('gantipassword.save')}}" class="forms-sample">
                                @csrf
                               

                                                            
                                <div class="form-group">
                                    <label for="password_lama">Password Lama</label>
                                    <div class="input-group mb-3">
                                    <input type="password"  name="password_lama" class="form-control" id="password_lama"
                                        placeholder="Password Lama">
                                    </div>
                                    @error('password_lama')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password_baru">Password Baru</label>
                                    <div class="input-group mb-3">
                                    <input type="password"  name="password_baru" class="form-control" id="password_baru"
                                        placeholder="Password Baru">
                                    </div>
                                    @error('password_baru')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>


                                <div class="form-group">
                                    <label for="konfir_password">Konfirmasi Password</label>
                                    <div class="input-group mb-3">
                                    <input type="password"  name="konf_password" class="form-control" id="konf_password"
                                        placeholder="Konfirmasi Password">
                                    </div>
                                    @error('konf_password')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>  

                               
                                
                                <button type="submit" class="btn btn-primary mr-2"> Simpan </button>
                                <button type="reset" class="btn btn-light">Batal</a>
                        </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>


        
        @endsection

    @section('footer_scripts')
      <script>
     
    </script>
    @endsection