 <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="container">
              
              <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2022 <a href="#" target="_blank">Pos Mini PT. Majoo Teknologi</a>. All rights reserved.</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Pos Mini PT. Majoo Teknologi <i class="mdi mdi-heart text-danger"></i></span>
              </div>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>

 <!-- page-body-wrapper ends -->
 </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('assets/js/jquery.3.2.1.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->

   
    

    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    

    <script src="{{asset('assets/vendors/dropify/dropify.min.js')}}"></script>
     <!-- Sweat Alert -->
    <script src="{{asset('assets/sweetalert/sweetalert.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/jquery.twbsPagination.min.js')}}"></script>
    <!-- inject:js -->
    <!-- <script src="{{asset('assets/js/off-canvas.js')}}"></script> -->
    <script src="{{asset('assets/js/hoverable-collapse.js')}}"></script>
    <!-- <script src="{{asset('assets/js/misc.js')}}"></script> -->
    <script src="{{asset('assets/js/settings.js')}}"></script>
    <!-- <script src="{{asset('assets/js/todolist.js')}}"></script> -->
    <!-- endinject -->
   
    <!-- End custom js for this page -->

    @yield('footer_scripts')
  </body>
</html>