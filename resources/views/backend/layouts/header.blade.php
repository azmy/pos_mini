<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Pos Mini PT. Majoo Teknologi</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.css"> -->
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendors/dropify/dropify.min.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('assets/vendors/jquery-bar-rating/css-stars.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('assets/css/demo_2/style.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/demo_3/style.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/demo_2/custom.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}" />
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <style>
    table.dataTable, table.dataTable th, table.dataTable td{
      font-size:12px!important;
    }
    .dataTables_wrapper .dataTables_length, 
    .dataTables_wrapper .dataTables_filter, 
    .dataTables_wrapper .dataTables_info, 
    .dataTables_wrapper .dataTables_processing, 
    .dataTables_wrapper .dataTables_paginate {
      font-size:12px!important;
    }
    .col-12,.col-lg-12{
      padding-right:5px !important;
      padding-left:5px !important;
    }
    @media (min-width: 992px){
    .container-scroller .content-wrapper {
    max-width: 1400px;
    }
    }
  </style>
  @yield('styles')
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_horizontal-navbar.html -->
      <div class="horizontal-menu">
        <nav class="navbar top-navbar col-lg-12 col-12 p-0">
          <div class="container">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
              <a class="navbar-brand brand-logo" href="#">
                <!-- <img src="{{asset('assets/images/logo.svg')}}" alt="logo" /> -->
                Pos Mini PT. Majoo Teknologi
                <span class="font-12 d-block font-weight-light">Majoo Plaza
Jl. Prapanca Raya No.25
Jakarta - Indonesia, 12160 </span>
              </a>
              <a class="navbar-brand brand-logo-mini" href="#"><img src="{{asset('assets/images/logo-mini.svg')}}" alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
             
              <ul class="navbar-nav navbar-nav-right">
                <li class="nav-item nav-profile dropdown">
                  <a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                    <!-- <div class="nav-profile-img">
                      <img src="{{asset('assets/images/faces-clipart/pic-4.png')}}" alt="image" />
                    </div> -->
                    <div class="nav-profile-text">
                      <p class="text-black font-weight-semibold m-0">  {{@Auth::user()->username}} </p>
                      <span class="font-13 online-color">online <i class="mdi mdi-chevron-down"></i></span>
                    </div>
                  </a>
                  <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" href="{{route('gantipassword.index')}}">
                      <i class="mdi mdi-cached mr-2 text-success"></i> Ganti Password </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="event.preventDefault();
						document.getElementById('logout-form').submit();">
                      <i class="mdi mdi-logout mr-2 text-primary"></i> Signout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        			@csrf
                  </form>
                  </div>
                </li>
              </ul>
              <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                <span class="mdi mdi-menu"></span>
              </button>
            </div>
          </div>
        </nav>
        @include('layouts/menu_header')
      </div>