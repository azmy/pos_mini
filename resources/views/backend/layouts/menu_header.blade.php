<nav class="bottom-navbar">
          <div class="container">
            <ul class="nav page-navigation">
              <li class="nav-item">
                <a class="nav-link" href="{{route('/')}}">
                  <i class="mdi mdi-compass-outline menu-icon"></i>
                  <span class="menu-title">Dashboard</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="mdi mdi-monitor-dashboard menu-icon"></i>
                  <span class="menu-title">Master</span>
                  <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                  <ul class="submenu-item">
                  <li class="nav-item">
                      <a class="nav-link" href="{{route('kategori.index')}}"><i class="mdi mdi-view-list"></i> Kategori</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('product.index')}}"><i class="mdi mdi-cart-outline"></i> Product</a>
                    </li>
                   
                    </ul>
                </div>
              </li>

            

              

            

            


              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="mdi mdi-monitor-dashboard menu-icon"></i>
                  <span class="menu-title">Setting</span>
                  <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                  <ul class="submenu-item">
                 
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('menu.menus')}}"><i class="mdi mdi-apps"></i> Menu</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('module.modules')}}"><i class="mdi mdi-apps"></i> Modul</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('role.roles')}}"><i class="mdi mdi-account"></i> Level Pengguna</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{route('user.users')}}"><i class="mdi mdi-account"></i> User</a>
                    </li>
                    </ul>
                </div>
              </li>
            
            </ul>
          </div>
        </nav>