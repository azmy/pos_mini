@extends('template')
@section('main')
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Setting</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Hak Akses</a></li>
                        <li class="breadcrumb-item active" aria-current="page">List Hak Akses</li>
                    </ol>
                </nav>
            </div>

            <div class="row">
            @if(Session::has('gagal'))
            <div class="alert alert-danger">
                {{Session::get('gagal')}}
            </div>
            @endif

            @if(Session::has('sukses'))
            <div class="alert alert-success">
                {{Session::get('sukses')}}
            </div>
            @endif     
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        
                      
                      <div class="table-responsive">
                      <form method="post" action="{{route('role.saveaccess',Session::get('role'))}}">
                      @csrf
                     
                      <button type="submit" class="pull-right btn btn-primary btn-sm btn-icon-text" style="margin-bottom:20px;"><i class="fa fa-save"></i> Save</button>
                     
                      <table id="basic-datatables" class="display table-bordered table table-striped table-hover"  width="100%">
                      <thead>
                      <tr>
                      <th width="8px;" align="center">No</th>
                      <th>Name</th>
                      <th style="width:10px">Read</th>
                      <th style="width:10px">Add</th>
                      <th style="width:10px">Edit</th>
                      <th style="width:10px">Delete</th>
                      </tr>
                      </thead>
                      <tbody>
                      @php
                      $repeat_value = explode(',', @$menu->menu_id)
                      @endphp
                      @foreach($module as $modules)    
                      @php $check = in_array($modules->menu_id,$repeat_value)?'checked':'' @endphp   
                        <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><input type="hidden" value="{{$modules->id}}" name="id[{{$modules->id}}]">{{$modules->name}} <small><br>slug : {{$modules->slug}}</small></td>
                      @if($modules->permission_roles->count()>0)
                      @foreach($modules->permission_roles as $v)
                      @if($v->module_id==$modules->id && $v->role_id==Session::get('role')) 
                        <td style="text-align:center;"><input type="checkbox" style="display:none" class="menu" value="{{$modules->menu_id}}" name="menu_id[]"  {{@$check}}><input class="read" type="checkbox" value="1" name="read[{{$modules->id}}]" {{in_array(1,array($v->can_access))?'checked':''}}></td>
                        <td style="text-align:center;"><input type="checkbox" value="1" name="add[{{$modules->id}}]"  {{in_array(1,array($v->can_create))?'checked':''}}></td>
                        <td style="text-align:center;"><input type="checkbox" value="1" name="edit[{{$modules->id}}]" {{in_array(1,array($v->can_edit))?'checked':''}}></td>
                        <td style="text-align:center;"><input type="checkbox" value="1" name="delete[{{$modules->id}}]" {{in_array(1,array($v->can_delete))?'checked':''}}></td>
                        </tr> 
                      @elseif($v->module_id!=$modules->id)
                      <td style="text-align:center;"><input type="checkbox" style="display:none" class="menu" value="{{$modules->menu_id}}" name="menu_id[]"><input class="read" type="checkbox" value="1" name="read[{{$modules->id}}]" ></td>
                      <td style="text-align:center;"><input type="checkbox" value="1" name="add[{{$modules->id}}]"  ></td>
                      <td style="text-align:center;"><input type="checkbox" value="1" name="edit[{{$modules->id}}]" ></td>
                      <td style="text-align:center;"><input type="checkbox" value="1" name="delete[{{$modules->id}}]"></td>
                      </tr>
                      @endif
                      @endforeach
                     
                      @endif
                      @if($modules->permission_roles->where('role_id',Session::get('role'))->count()==0)
                      <td style="text-align:center;"><input type="checkbox" style="display:none" class="menu" value="{{$modules->menu_id}}" name="menu_id[]"><input class="read" type="checkbox" value="1" name="read[{{$modules->id}}]" ></td>
                      <td style="text-align:center;"><input type="checkbox" value="1" name="add[{{$modules->id}}]"  ></td>
                      <td style="text-align:center;"><input type="checkbox" value="1" name="edit[{{$modules->id}}]" ></td>
                      <td style="text-align:center;"><input type="checkbox" value="1" name="delete[{{$modules->id}}]"></td> 
                      @endif
                      @endforeach
                      </tr>
                      </tbody>
                    </table>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('footer_scripts')
<script>
$(document).ready(function() { 
$('input.read').on('click', function() {
  if($(this).is(":checked")){
  $(this).parent().parent().find('.menu').prop('checked', true);
  $(this).prop('checked', true)
  }else{
  $(this).parent().parent().find('.menu').prop('checked', false);
  }
});
});
</script>
@endsection
