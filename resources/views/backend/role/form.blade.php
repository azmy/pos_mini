@extends('template')
@section('main')
<style>
    .has-error{
    padding-top:10px;
    font-size:9px !important;
    }
</style>
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Form Level Pengguna</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Level Pengguna</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> @if(!empty(@$kategori->id)) Edit @else Add @endif Level Pengguna </li>
                    </ol>
                </nav>
            </div>



            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Form Level Pengguna</h4>
                            <p class="card-description">Digunakan untuk menambah atau mengedit level pengguna</p>
                            <form method="POST" action="{{route('role.save', @$role->id)}}" class="forms-sample">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Level</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$role->name}}" name="name" class="form-control" id="name"
                                        placeholder="Level Pengguna">
                                    </div>
                                    @error('name')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="name">Deskripsi</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$role->description}}" name="description" class="form-control" id="description"
                                        placeholder="Deskripsi">
                                    </div>
                                    @error('description')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>
                                
                                <button type="submit" class="btn btn-primary mr-2"> Simpan </button>
                                <a href="{{route('role.roles')}}" class="btn btn-light">Batal</a>
                        </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>


        
        @endsection

    @section('footer_scripts')
      <script>
     
    </script>
    @endsection