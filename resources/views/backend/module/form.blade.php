@extends('template')
@section('main')
<style>
    .has-error{
    padding-top:10px;
    font-size:9px !important;
    }
</style>
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Setting</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Module</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> @if(!empty(@$module->id)) Edit @else Add @endif Module </li>
                    </ol>
                </nav>
            </div>



            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Form Module</h4>
                            <p class="card-description">Digunakan untuk menambah atau mengedit module</p>
                            <form method="POST" action="{{route('module.save', @$module->id)}}" class="forms-sample">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Nama Module</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$module->name}}" name="name" class="form-control" id="name"
                                        placeholder="Nama Menu">
                                    </div>
                                    @error('name')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="icon">Menu</label>
                                    <div class="input-group mb-3">
                                    <select class="form-control" id="menu" name="menu">
                                    <option>Pilih</option>
                                    @foreach($menu as $menus)
                                    {{@$sel = $menus->id == @$module->menu_id ? 'selected':''}}
                                    <option value="{{$menus->id}}" {{$sel}}>{{@$menus->nama}}</option>
                                    @endforeach
                                    </select>
                                    @error('menu')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="icon">Url <small>(Tanpa nama Domain)</small></label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$module->slug}}" name="slug" class="form-control" id="slug"
                                        placeholder="Url">
                                    </div>
                                    @error('slug')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <label for="icon">Deskripsi</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$module->description}}" name="description" class="form-control" id="description"
                                        placeholder="Deskripsi">
                                    </div>
                                    @error('description')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="input-group mb-3" style="margin-left:20px !important;font-size:11pt;">
                                    @if(!empty(@$module->id))
                                    <input class="form-check-input" type="checkbox" value="{{@$module->active}}" name="active" {{(@$module->active==1) ? 'checked':''}}> Active
                                    @else
                                    <input class="form-check-input" type="checkbox" value="1" name="active"> Active
                                    @endif
                                    </div>
                                    @error('active')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>
                                

                                <button type="submit" class="btn btn-primary mr-2"> Simpan </button>
                                <a href="{{route('module.modules')}}" class="btn btn-light">Batal</a>
                        </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>


        
        @endsection

    @section('footer_scripts')
    <script>
         $("#menu").change(function(){
          $('#slug').val('');
            $.ajax({
                url: "{{ route('menu.searchslug') }}/" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#slug').val(data);
                }
            });
        });
    </script>
    @endsection