@extends('template')
@section('main')
<style>
    .has-error{
    padding-top:10px;
    font-size:9px !important;
    }
</style>
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">Setting</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Menu</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> @if(!empty(@$menu->id)) Edit @else Add @endif Menu </li>
                    </ol>
                </nav>
            </div>



            <div class="row">

                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Form Menu</h4>
                            <p class="card-description">Digunakan untuk menambah atau mengedit menu</p>
                            <form method="POST" action="{{route('menu.save', @$menu->id)}}" class="forms-sample">
                                @csrf
                                <div class="form-group">
                                    <label for="parent_id">ID Induk</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$menu->id_induk}}" name="parent_id" class="form-control" id="parent_id"
                                        placeholder="Kategori">
                                    </div>
                                    @error('parent_id')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="name">Nama Menu</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$menu->nama}}" name="name" class="form-control" id="name"
                                        placeholder="Nama Menu">
                                    </div>
                                    @error('name')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="icon">Icon</label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$menu->icon}}" name="icon" class="form-control" id="icon"
                                        placeholder="Nama Icon">
                                    </div>
                                    @error('icon')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="icon">Url <small>(Tanpa nama Domain)</small></label>
                                    <div class="input-group mb-3">
                                    <input type="text" value="{{@$menu->url}}" name="slug" class="form-control" id="slug"
                                        placeholder="Url">
                                    </div>
                                    @error('slug')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <label for="icon">Urutan</label>
                                    <div class="input-group mb-3">
                                    <input type="number" value="{{@$menu->order}}" name="order" class="form-control" id="order"
                                        placeholder="Order">
                                    </div>
                                    @error('order')
                                    <span class="has-error has-feedback error text-danger" role="alert">
                                        <label for="errorInput">{{ $message }}</label>
                                    </span>
                                    @enderror
                                </div>
                                

                                <button type="submit" class="btn btn-primary mr-2"> Simpan </button>
                                <a href="{{route('menu.menus')}}" class="btn btn-light">Batal</a>
                        </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>


        
        @endsection

    @section('footer_scripts')
      <script>
     
    </script>
    @endsection