
<!DOCTYPE html>
<html lang="en">
  <head>
 
  
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Pos Mini PT. Majoo Teknologi</title>
    <!-- plugins:css -->
    <!-- <link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/flag-icon-css/css/flag-icon.min.css')}}"> -->
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('assets/css/demo_3/style.css')}}">
    <!-- End layout styles -->
    <!-- <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" /> -->
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo text-center">
                  <!-- <img src="{{asset('assets/images/logo.svg')}}"> -->
                  <h1><b>Pos Mini PT. Majoo Teknologi</b></h1>
                </div>
                <!-- <h4>Hello! let's get started</h4> -->
                <h6 class="font-weight-light text-center">Masukkan Username dan Password</h6>
                <form class="pt-3"  method="POST" action="{{ route('login') }}">
                @csrf
                  <div class="form-group">
                    <input type="text"  name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username" value="{{ old('username') }}">
                    @error('username')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <input type="password"  name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="mt-3">
                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-semibold auth-form-btn">Login</button>
                  </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
   
  </body>
</html>