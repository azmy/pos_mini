-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pos_mini
CREATE DATABASE IF NOT EXISTS `pos_mini` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pos_mini`;

-- Dumping structure for table pos_mini.kategori
CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_mini.kategori: ~4 rows (approximately)
DELETE FROM `kategori`;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` (`id`, `nama`, `created_at`, `updated_at`) VALUES
	(11, 'Ecomerce', '2022-05-17 10:33:26', '2022-08-27 17:24:07'),
	(12, 'Point Of Sale', '2022-05-24 09:22:18', '2022-08-27 17:24:38'),
	(13, 'Website Personal', '2022-08-27 17:25:24', '2022-08-27 17:25:24'),
	(14, 'Website Perusahaan', '2022-08-27 17:25:40', '2022-08-27 17:25:40');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;

-- Dumping structure for table pos_mini.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_induk` int(11) NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table pos_mini.menu: ~7 rows (approximately)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `id_induk`, `nama`, `icon`, `url`, `order`, `created_at`, `updated_at`) VALUES
	(101, 0, 'Dashboard', 'fa-dashboard', '/admin', 1, NULL, NULL),
	(102, 0, 'Hak Akses', 'fa-edit', '#', 3, NULL, '2019-07-24 12:53:41'),
	(103, 102, 'Role', 'fa-unlock', 'admin/role', 3, NULL, '2019-07-23 07:59:07'),
	(104, 102, 'User', 'fa-users', 'admin/user', 4, NULL, '2019-07-23 07:59:15'),
	(105, 102, 'Module', 'fa-navicon', 'admin/module', 2, NULL, '2019-07-23 07:58:59'),
	(110, 102, 'Menu', 'fa-list', 'admin/menu', 1, NULL, '2022-06-14 06:37:57'),
	(111, 0, 'Kasir', 'fa fa-file', 'admin/kasir', 0, '2022-06-14 04:41:46', '2022-06-14 04:41:46');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table pos_mini.menu_role
CREATE TABLE IF NOT EXISTS `menu_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `menu_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_mini.menu_role: ~2 rows (approximately)
DELETE FROM `menu_role`;
/*!40000 ALTER TABLE `menu_role` DISABLE KEYS */;
INSERT INTO `menu_role` (`id`, `role_id`, `menu_id`) VALUES
	(88, 2, '101'),
	(92, 3, '101,111');
/*!40000 ALTER TABLE `menu_role` ENABLE KEYS */;

-- Dumping structure for table pos_mini.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.migrations: ~1 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table pos_mini.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `menu_id` int(11) unsigned DEFAULT NULL,
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.module: ~6 rows (approximately)
DELETE FROM `module`;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `menu_id`, `slug`, `active`) VALUES
	(2, 'Halaman Module', 'Edit Users', 'edit existing users', '2019-07-05 10:16:26', '2019-07-20 04:27:09', 105, 'admin/module', 1),
	(3, 'Halaman Users', 'create,edit,delete', 'Page User', '2019-07-06 00:00:00', '2019-07-20 04:22:02', 104, 'admin/user', 1),
	(6, 'Dashboard', NULL, 'Halaman Home', '2019-07-23 00:29:30', '2019-07-23 00:29:36', 101, 'admin', 1),
	(7, 'Hak Akses', NULL, 'Halaman Hak Akses User', '2019-07-23 00:31:14', '2019-07-23 00:31:19', 102, '#', 1),
	(8, 'Halaman Role Access', NULL, 'ok', '2019-07-31 07:24:01', '2019-07-31 07:24:01', 103, 'admin/role', 1),
	(10, 'Kasir', NULL, 'Digunakan untuk form transaksi', '2022-06-15 10:35:57', '2022-06-15 10:35:57', 111, 'admin/kasir', 1);
/*!40000 ALTER TABLE `module` ENABLE KEYS */;

-- Dumping structure for table pos_mini.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table pos_mini.permission_role
CREATE TABLE IF NOT EXISTS `permission_role` (
  `module_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `can_access` int(2) DEFAULT NULL,
  `can_create` int(2) DEFAULT NULL,
  `can_edit` int(2) DEFAULT NULL,
  `can_delete` int(2) DEFAULT NULL,
  PRIMARY KEY (`module_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.permission_role: ~10 rows (approximately)
DELETE FROM `permission_role`;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`module_id`, `role_id`, `can_access`, `can_create`, `can_edit`, `can_delete`) VALUES
	(2, 2, 0, 0, 0, 0),
	(2, 3, 0, 0, 0, 0),
	(3, 2, 0, 0, 0, 0),
	(3, 3, 0, 0, 0, 0),
	(6, 2, 1, 0, 0, 0),
	(6, 3, 1, 0, 0, 0),
	(7, 2, 0, 0, 0, 0),
	(7, 3, 0, 0, 0, 0),
	(8, 3, 0, 0, 0, 0),
	(10, 3, 1, 1, 1, 1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping structure for table pos_mini.permission_user
CREATE TABLE IF NOT EXISTS `permission_user` (
  `permission_id` int(10) unsigned NOT NULL,
  `users_id` int(10) unsigned NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`users_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`,`users_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.permission_user: ~3 rows (approximately)
DELETE FROM `permission_user`;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
INSERT INTO `permission_user` (`permission_id`, `users_id`, `user_type`) VALUES
	(1, 1, 'Admin'),
	(2, 1, 'Admin'),
	(3, 1, 'Admin');
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;

-- Dumping structure for table pos_mini.produk
CREATE TABLE IF NOT EXISTS `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) NOT NULL DEFAULT '0',
  `deskripsi` text,
  `harga` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `foto` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_kategori` (`id_kategori`),
  CONSTRAINT `FK_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table pos_mini.produk: ~11 rows (approximately)
DELETE FROM `produk`;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` (`id`, `nama`, `deskripsi`, `harga`, `id_kategori`, `foto`, `created_at`, `updated_at`) VALUES
	(18, 'Majoo Pro', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi eget mauris pharetra et ultrices neque ornare.</p>', 2750000, 12, 'standard_repo.png', '2022-08-28 09:05:41', '2022-08-28 09:05:41'),
	(19, 'Majoo Advance', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi eget mauris pharetra et ultrices neque ornare.</p>', 2750000, 12, 'paket-advance.png', '2022-08-28 09:06:38', '2022-08-28 09:06:38'),
	(20, 'Majoo Lifestyle', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi eget mauris pharetra et ultrices neque ornare.</p>', 2750000, 12, 'paket-lifestyle.png', '2022-08-28 09:07:14', '2022-08-28 09:07:14'),
	(21, 'Majoo Desktop', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi eget mauris pharetra et ultrices neque ornare.</p>', 2750000, 12, 'paket-desktop.png', '2022-08-28 09:07:58', '2022-08-28 09:07:58');
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;

-- Dumping structure for table pos_mini.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.roles: ~3 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', NULL, 'Administrator', '2019-07-17 03:55:11', '2019-07-17 03:55:11'),
	(2, 'Operator', NULL, 'Operator', '2019-07-17 03:55:42', '2019-07-17 03:55:42'),
	(3, 'Kasir', NULL, 'Kasir', '2019-07-17 03:55:52', '2022-06-14 01:28:09');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table pos_mini.role_user
CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission_id` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.role_user: ~0 rows (approximately)
DELETE FROM `role_user`;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;

-- Dumping structure for table pos_mini.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pos_mini.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `active`, `role_id`, `created_at`, `updated_at`) VALUES
	(1, 'Azmi', 'Azmi', 'azmysky27@gmail.com', '$2a$12$girGqSE7j5WeAElLBGkJ0ObxmMYV7CAv3hxifRjxtfkj474h2quxi', NULL, 1, 1, '2019-07-19 00:00:00', '2022-06-26 13:25:35'),
	(2, 'budi', 'budi', 'budi@gmail.com', '$2y$10$WRadkq/.l9OVDHk9qdK2yO8SX0PTV.LJE5E0Ueb4IEpj3fsrWDkQu', NULL, 1, 3, '2019-07-19 00:00:00', '2022-06-26 10:28:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
